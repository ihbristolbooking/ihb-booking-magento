#
# Application-form feature
#

Feature: Apply for English

  Scenario: As a customer i wish to send an 'ap
    Given I am on "/english-courses/apply"
    Then I should see "Apply for an English Course online with I H Bristol"
    Then I select "57" from "field_learn_english_course_type[und]"
    And I select "0" from "field_agent_booking[und]"
    And I fill in "field_course_dates[und][0][value][date]" with "07/04/2018"
    And I fill in "field_course_dates[und][0][value2][date]" with "07/02/2019"

    And I select "0" from "field_embassy_sponsor[und]"
    And I fill in "field_first_name[und][0][value]" with "Testing"
    And I fill in "field_surname[und][0][value]" with "User"
    And I select "92" from "field_sex[und]"
    And I select "16" from "field_date_of_birth[und][0][value][day]"
    And I select "3" from "field_date_of_birth[und][0][value][month]"
    And I select "2000" from "field_date_of_birth[und][0][value][year]"
    And I fill in "field_mother_tongue[und][0][value]" with "English"
    And I fill in "field_nationality[und][0][value]" with "English"
    And I fill in "field_passport_id_number[und][0][value]" with "1234567890"
    And I fill in "field_email[und][0][email]" with "office@brightstorm.co.uk"
    # contact details

    And I fill in "field_address[und][0][thoroughfare]" with "thoroughfare"
    And I fill in "field_address[und][0][locality]" with "city"
    And I fill in "field_uk_address[und][0][postal_code]" with "SN12 1AZ"

    And I fill in "field_uk_address[und][0][thoroughfare]" with "thoroughfare UK Address"
    And I fill in "field_uk_address[und][0][locality]" with "locality UK Address"

    And I fill in "field_emergency_name[und][0][value]" with "Emergency Name"
    And I fill in "field_emergency_email[und][0][email]" with "office1@brightstorm.co.uk"
    And I fill in "field_emergency_telephone_any[und][0][value]" with "01264 123123"

    And I select "35" from "field_level[und]"
    And I select "I do not require accommodation" from "field_accommodation_type[und]"
    And I select "88" from "field_payment_methods[und]"
    And I select "76" from "field_lead_generation[und]"

    And I select "1" from "field_terms[und]"

    And I select "No" from "field_process[und]"

    And I press "Submit"
    Then I should not see "Which Course Do You Want To Take? field is required."
    Then I should not see "Are you booking through an Agent field is required."
    Then I should not see "A valid date is required for Course Dates Start date."
    Then I should not see "A valid date is required for Course Dates End date."
    Then I should not see "Are you sponsored by an embassy? field is required."
    Then I should not see "First Name field is required."
    Then I should not see "Surname field is required."
    Then I should not see "Sex field is required."
    Then I should not see "A valid date is required for Date of Birth."
    Then I should not see "Mother Tongue field is required."
    Then I should not see "Passport / ID number field is required."
    Then I should not see "Email field is required."
    Then I should not see "Address 1 field is required."
    Then I should not see "City field is required."
    Then I should not see "Name field is required."
    Then I should not see "Telephone field is required."
    Then I should not see "Email field is required."
    Then I should not see "Your Level field is required."
    Then I should not see "Accommodation type field is required."
    Then I should not see "Payment Methods field is required."
    Then I should not see "Where did you hear about International House Bristol? field is required."
    Then I should not see "I have read and accepted the Terms & Conditions field is required."
    Then I should not see "Process my booking immediately field is required."
    Then I should not see "Nationality field is required."
    Then I should not see "By agreeing that IH Bristol can process your booking immediately you confirm you are aware that, in the event of a cancellation, IH Bristol may charge you for services already rendered. If you select 'No' in the 'Process my booking immediately' your booking will be processed by us after 14 days (cooling off period)."
    #And print last response

