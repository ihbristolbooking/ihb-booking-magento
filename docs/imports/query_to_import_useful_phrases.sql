select
	exp_channel_data.entry_id as `guid`,
	exp_channel_titles.url_title as `Url`,
	exp_channel_titles.title as `Title`,
	exp_channel_data.field_id_66 as `Introduction`,
	exp_channel_data.field_id_112 as `Image`,
	GROUP_CONCAT(exp_categories.cat_name SEPARATOR '*') as `Category`,
	GROUP_CONCAT(exp_matrix_data.col_id_6 SEPARATOR '*') as `Expressions`,
	GROUP_CONCAT(exp_matrix_data.col_id_7 SEPARATOR '*') as `Usage`
from
	exp_channel_data,
	exp_category_posts,
	exp_categories,
	exp_matrix_data,
	exp_channel_titles

where
 	exp_channel_data.entry_id = exp_matrix_data.entry_id
AND
	exp_channel_data.entry_id = exp_category_posts.entry_id
AND
	exp_channel_data.entry_id = exp_channel_titles.entry_id
AND
	exp_channel_data.channel_id = 10
AND
	exp_category_posts.cat_id = exp_categories.cat_id
group by
  exp_channel_data.entry_id
  
INTO OUTFILE '/tmp/usefulphrases.csv'
FIELDS TERMINATED BY '|'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
