<?php
// declare output file
$file = 'data/word_formation.csv';
$rowsoutput = null;


$username = "root";
$password = "";
$hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";

$selected = mysql_select_db("ihbristol",$dbhandle) 
  or die("Could not select examples");

$query = <<<EOT
select
CONCAT(cd.entry_id, '-', iq.question_id) as guid,
cd.entry_id,
ect.title as title,
iq.qg_id as iLearn,
iq.question_id,
iq.row_text as question,
iq.op_word
from 
exp_ilearn_questions as iq
JOIN
exp_ilearn_question_groups AS eiqg
on
eiqg.id = iq.qg_id

JOIN 
exp_channel_data as cd
on
cd.field_id_92 = iq.qg_id 

JOIN
exp_channel_titles as ect
on 
ect.entry_id = cd.entry_id
WHERE
eiqg.question_type = 'swf'
EOT;

//execute the SQL query and return records
$result = mysql_query($query);
//fetch tha data from the database
while ($row = mysql_fetch_array($result)) {

	$data = array();
	
	$row['guid'] = $row['entry_id'].'-'.$row['question_id'];
 
	$d[$row['guid']]['guid'] = $row['guid'];
	$d[$row['guid']]['title'] = $row['title'];
	$d[$row['guid']]['entry_id'] = $row['entry_id'];
	$d[$row['guid']]['iLearn'] = $row['iLearn'];
	$d[$row['guid']]['question'] = $row['question'];
	$d[$row['guid']]['evaluation code'] = 4;
	$d[$row['guid']]['answer'] = 'answer';
	$d[$row['guid']]['op_word'] = $row['op_word'];

	}

	$answers = array();
	$counter = -1;
	$current = null;
	$thisLine = null;

	foreach($d as $i => $row){

		if($current !=$row['entry_id']){
			$counter = -1;
		}

		$thisLine = $row['question'];	

	/*	if($counter==0){

			$replace = '*0/'.$answers[$row['entry_id'].'-0']['answer'].'/'.$answers[$row['entry_id'].'-0']['op_word'].'*';

			$example = str_replace('{0}',$replace,$answers[$row['entry_id'].'-0']['question']);
 
			$thisLine = $example;

		} else {
			$thisLine = $row['question'];	

			// delete the example row
			unset($answers[$row['entry_id'].'-0']);

		}
*/
		$pos = strpos($thisLine, '{');

		if(isset($guid) and ($pos === false)){
			$answers[$guid]['question'] .= strlen(trim($thisLine))>2 ? ' '.$thisLine : "<br class='breakline' />";
		} else {
			$counter ++;

			$answer_sql = 'select a as answer from exp_ilearn_answers as la where qg_id = '.$row['iLearn'].' and answer_id = '.$counter;

			$guid = $row['entry_id'].'-'.$counter;

			$answers[$guid]['guid'] = $guid;
			$answers[$guid]['title'] = '"'.$row['title'].'"';
			$answers[$guid]['question'] = $thisLine;
			$answers[$guid]['iLearn'] = $row['iLearn'];
			$answers[$guid]['entry_id'] = $row['entry_id'];
			$answers[$guid]['op_word'] = $row['op_word'];
			$answers[$guid]['question_id'] = $counter;
			$answers[$guid]['evaluation code'] = 1;
			$sub = mysql_query($answer_sql);
			$res = mysql_fetch_row($sub);

			$answers[$guid]['answer'] = $res[0];

			$current = $row['entry_id'];
		};


		if($counter ==1){
			$exguid = $row['entry_id'].'-0';
			$replace = '*0/'.$answers[$exguid]['answer'].'/'.$answers[$exguid]['op_word'].'*';
			$example = str_replace('{0}',$replace,$answers[$exguid]['question']);
			$answers[$guid]['question']  = $example.' '.$answers[$guid]['question'];
		}

		if($counter > 1)
			unset($answers[$exguid]);

	}

	foreach ($answers as $key => $row) {
		# code...
		$row['question'] = '"'.str_replace(array('  ','<br/> '),array(' ','<br/>'),trim($row['question'])).'"';

	//	echo $row['row_text']."\n";
		$rows = array_values($row);


		$rowsoutput .= implode(',', $rows)."\n";
	}

	$current = implode(array_keys($row),',')."\n";
	$output = $current.$rowsoutput;

	file_put_contents($file, $output);
	mysql_close($dbhandle);

?>