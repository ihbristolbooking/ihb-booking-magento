select
	exp_channel_data.entry_id as `Guid`,
	exp_channel_titles.url_title as `url_title`,
	exp_channel_titles.title as `Title`,
	exp_calendar_events.start_date AS `Date`,
	exp_calendar_events.start_month AS `Month`,
	exp_calendar_events.start_day AS `Day`,
	exp_channel_data.field_id_69 as Explanation,
	exp_channel_data.field_id_130 as Youtube,
	exp_channel_data.field_id_142 as Image,

	GROUP_CONCAT(exp_matrix_data.col_id_11 SEPARATOR ' ') as `Written`,
	GROUP_CONCAT(exp_matrix_data.col_id_10 SEPARATOR ' ') as `Spoken`,
	GROUP_CONCAT(exp_matrix_data.col_id_12 SEPARATOR ' ') as `Formal`,
	GROUP_CONCAT(exp_matrix_data.col_id_13 SEPARATOR ' ') as `Informal`,
	GROUP_CONCAT(exp_matrix_data.col_id_8 SEPARATOR '*') as `Example1`,
	GROUP_CONCAT(exp_matrix_data.col_id_9 SEPARATOR '*') as `Example2`
from
	exp_channel_data,
	exp_matrix_data,
	exp_channel_titles,
	exp_calendar_events
where
 	exp_channel_data.entry_id = exp_matrix_data.entry_id
AND
	exp_channel_data.entry_id = exp_calendar_events.entry_id
AND
	exp_channel_data.entry_id = exp_channel_titles.entry_id
AND
	exp_channel_titles.year > 2011
AND 
	exp_channel_titles.entry_id < 568
group by
  exp_channel_data.entry_id

INTO OUTFILE '/tmp/phraseoftheday.csv'
FIELDS TERMINATED BY '|'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
