<?php
// declare output file
$file = 'data/gapped_sentence_titles.csv';
$rowsoutput = null;

$username = "root";
$password = "";
$hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";

$selected = mysql_select_db("ihbristol",$dbhandle) 
  or die("Could not select examples");

$query = <<<EOT
select 
ia.id,
CONCAT(cd.entry_id, '-', ia.id) as guid,
ect.title,
cd.entry_id,
ia.qg_id as iLearn,
ia.answer_id as question_id,
ia.a as answer,
iq.row_text
from
exp_ilearn_answers as ia 
JOIN
exp_ilearn_question_groups AS eiqg
on
eiqg.id = ia.qg_id

JOIN 
exp_channel_data as cd
on
cd.field_id_92 = ia.qg_id 

JOIN
exp_channel_titles AS ect
on
cd.entry_id = ect.entry_id

JOIN 
exp_ilearn_questions as iq
on
iq.qg_id = ia.qg_id and iq.question_id = ia.answer_id
and
eiqg.question_type = 'sgs'
WHERE 
iq.question_id = 0
EOT;

//execute the SQL query and return records
$result = mysql_query($query);
//fetch tha data from the database
while ($row = mysql_fetch_array($result)) {

	$data = array();
	
	$row['guid'] = $row['entry_id'].'-'.$row['question_id'];

	$row['row_text'] = str_replace('0. ______', '{0} ', $row['row_text']);
	$row['row_text'] = str_replace("\n", "^", $row['row_text']);
	$row['row_text'] = str_replace('  ', ' ', $row['row_text']);

	$d[$row['guid']]['guid'] = $row['guid'];
	$d[$row['guid']]['title'] = 'Quiz Page - '.$row['title'];
	$d[$row['guid']]['entry_id'] = $row['entry_id'];
	$d[$row['guid']]['iLearn'] = $row['iLearn'];
	$d[$row['guid']]['example'] = '"<example><sentence>'.$row['row_text'].'</sentence><answer>'.$row['answer'].'</answer></example>"';
	}

	foreach ($d as $key => $row) {
		# code...
		$rows = array_values($row);

//		var_export($rows);

		$rowsoutput .= implode('|', $rows)."\n";
	}

	$headers = array_keys($row);

	$current = implode($headers,'|')."\n";
	$output = $current.$rowsoutput;

	file_put_contents($file, $output);
	mysql_close($dbhandle);

?>