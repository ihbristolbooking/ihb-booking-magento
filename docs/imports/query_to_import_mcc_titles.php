<?php
// declare output file
$file = 'data/multiplechoice_quiz_page.csv';
$rowsoutput = null;


$username = "root";
$password = "";
$hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";

$selected = mysql_select_db("ihbristol",$dbhandle) 
  or die("Could not select examples");

$query = <<<EOT
select
CONCAT(cd.entry_id, '-', iq.question_id) as guid,
cd.entry_id,
ect.title as title,
iq.qg_id as iLearn,
iq.row_text as question,
ia.a as option_a,
ia.b as option_b,
ia.c as option_c,
ia.d as option_d,
ia.correct as 'correct'
from 
exp_ilearn_questions as iq
JOIN
exp_ilearn_question_groups AS eiqg
on
eiqg.id = iq.qg_id

JOIN 
exp_channel_data as cd
on
cd.field_id_92 = iq.qg_id 

JOIN
exp_ilearn_answers as ia
on 
cd.field_id_92 = ia.qg_id and ia.answer_id = 0

JOIN
exp_channel_titles as ect
on 
ect.entry_id = cd.entry_id
WHERE
eiqg.question_type = 'mcc' or eiqg.question_type = 'smc'

EOT;

//and 
//ect.entry_id = 1857

//execute the SQL query and return records
$result = mysql_query($query);
//fetch tha data from the database
while ($row = mysql_fetch_array($result)) {

	$data = array();
	
	$row['guid'] = $row['entry_id'].'-1';

	$correct = !empty($row['correct']) ? $row['correct'] : null;

	$correct_example = !empty($correct) ? $row['option_'.$correct] : $row['option_c'];

	$row['question'] = str_replace('{0}','*0/'.$correct_example.'*',$row['question']);

	$row['question'] = str_replace(array("‘","’","\n"), array('&lsquo;','&rsquo;','<br/>'), $row['question']);

	$row['question'] = str_replace("\r", '', $row['question']);

	$answers = array($row['option_a'],$row['option_b'],$row['option_c'],$row['option_d']);

	$question0 = '"<example><options>'.implode('|',$answers).'</options><correct>'.$correct_example.'</correct></example>"';
 
	$d[$row['guid']]['guid'] = $row['guid'];
	$d[$row['guid']]['title'] = 'Quiz Page - '.$row['title'];
	$d[$row['guid']]['entry_id'] = $row['entry_id'];
	$d[$row['guid']]['iLearn'] = $row['iLearn'];
	$d[$row['guid']]['example'] = '"'.$row['question'].'"';
	$d[$row['guid']]['question0'] = $question0;

//	var_export($d[$row['guid']]);

	}

	foreach ($d as $key => $row) {
		# code...

	//	echo $row['row_text']."\n";
		$rows = array_values($row);


		$rowsoutput .= implode('|', $rows)."\n";
	}

	$current = implode(array_keys($row),'|')."\n";
	$output = $current.$rowsoutput;

	file_put_contents($file, $output);
	mysql_close($dbhandle);

?>