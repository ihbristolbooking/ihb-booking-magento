<?php
// declare output file
$file = 'data/keyword_transformation.csv';
$rowsoutput = null;


$username = "root";
$password = "";
$hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";

$selected = mysql_select_db("ihbristol",$dbhandle) 
  or die("Could not select examples");

$query = <<<EOT
select 
ia.id,
CONCAT(cd.entry_id, '-', ia.id) as guid,
ect.title,
cd.entry_id,
ia.qg_id as iLearn,
ia.answer_id as question_id,
ia.a,
ia.b,
iq.row_text,
iq.op_word,
iq.op_sentence
from
exp_ilearn_answers as ia 
JOIN
exp_ilearn_question_groups AS eiqg
on
eiqg.id = ia.qg_id

JOIN 
exp_channel_data as cd
on
cd.field_id_92 = ia.qg_id 

JOIN
exp_channel_titles AS ect
on
cd.entry_id = ect.entry_id

JOIN 
exp_ilearn_questions as iq
on
iq.qg_id = ia.qg_id and iq.question_id = ia.answer_id
and
eiqg.question_type = 'skt'
WHERE 
iq.question_id > 0
EOT;

//execute the SQL query and return records
$result = mysql_query($query);
//fetch tha data from the database
while ($row = mysql_fetch_array($result)) {

	$data = array();
	
	$row['guid'] = $row['entry_id'].'-'.$row['question_id'];

	$answer = implode(array('a' => $row['a'],'b'=> $row['b']),'|');

	$answers[$row['guid']][$answer] = $answer; 

	$d[$row['guid']]['row_text'] = $row['row_text'];


	if($row['question_id']>1)
		unset($d[$row['entry_id'].'-0']);


	$d[$row['guid']]['guid'] = $row['guid'];
	$d[$row['guid']]['title'] = $row['title'];
	$d[$row['guid']]['entry_id'] = $row['entry_id'];
	$d[$row['guid']]['question_id'] = $row['question_id'];
	$d[$row['guid']]['iLearn'] = $row['iLearn'];
	$d[$row['guid']]['a'] = $row['a'];

	$d[$row['guid']]['evaluation code'] = 4;
	$d[$row['guid']]['answer'] = '"'.implode(array_values($answers[$row['guid']]),'*').'"';
	$d[$row['guid']]['op_word'] = $row['op_word'];
	$d[$row['guid']]['op_sentence'] = '"'.$row['op_sentence'].'"';

//	var_export($d);

	}

	foreach ($d as $key => $row) {
		# code...


		$rows = array_values($row);

		$rows[0] = '"'.$rows[0].'"';

		unset($rows[6]);

	//	var_export($rows);

		$rowsoutput .= implode(',', $rows)."\n";
	}

	$headers = array_keys($row);

	unset($headers[6]);

	$current = implode($headers,',')."\n";
	$output = $current.$rowsoutput;

	file_put_contents($file, $output);
	mysql_close($dbhandle);

?>