<?php
// declare output file
$file = 'data/multichoice.csv';
$rows = null;


$username = "root";
$password = "";
$hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";

$selected = mysql_select_db("ihbristol",$dbhandle) 
  or die("Could not select examples");

$query = <<<EOT
select 
ia.id,
CONCAT(cd.entry_id, '-', ia.id) as guid,
ect.title,
cd.entry_id,
eiqg.form_type,
ia.qg_id as iLearn,
ia.answer_id as 'question_id',
ia.a,
ia.b,
ia.c,
ia.d,
ia.correct as 'correct',
iq.row_text as question
from
exp_ilearn_answers as ia 
JOIN 
exp_ilearn_questions as iq
on
ia.qg_id = iq.qg_id and ia.answer_id = iq.question_id
JOIN 
exp_channel_data as cd
on
cd.field_id_92 = ia.qg_id 
JOIN
exp_ilearn_question_groups AS eiqg
on
eiqg.id = ia.qg_id
JOIN
exp_channel_titles AS ect
on
cd.entry_id = ect.entry_id
where 
ia.answer_id > 0
and
(eiqg.question_type = 'mcc' or eiqg.question_type = 'smc')
order by cd.entry_id ASC
EOT;

//execute the SQL query and return records
$result = mysql_query($query);
//fetch tha data from the database
while ($row = mysql_fetch_array($result)) {

	$data = array();
	
	$data['guid'] = $row['guid'];
	$data['title'] = '"'.$row['title'].'"';
	$data['entry_id'] = $row['entry_id'];
	$data['question_id'] = $row['question_id'];
	$data['iLearn'] = $row['iLearn'];
	$data['question'] = '"'.$row['question'].'"';

	$correct = $row['correct'];

	$data['correct answer'] = $row[$correct];

	$answers = array('a' => $row['a'],'b'=> $row['b'],'c' => $row['c'],'d'=>$row['d']);

	unset($answers[$correct]);

		$i = 1;
		foreach($answers as $answer => $value){
			$data['wrong answer '.$i] = $value;
			$i++;
		}

	$rows .= implode(array_values($data),',')."\n";

	}

	$current = implode(array_keys($data),',')."\n";

	$output = $current.$rows;

	file_put_contents($file, $output);

	mysql_close($dbhandle);

?>