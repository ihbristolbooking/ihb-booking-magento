<?php


function ihbristol_form_user_register_form_alter(&$form, &$form_state, $form_id) {
	//$form['account']['name']['#access'] = FALSE;
}



function ihbristol_preprocess_html(&$vars) {
  foreach($vars['user']->roles as $role){
    $vars['classes_array'][] = 'role-' . drupal_html_class($role);
  }
}



function ihbristol_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  $breadcrumb = array();

  if (!empty($breadcrumb)) {
    // Adding the title of the current page to the breadcrumb.
    $breadcrumb[] = drupal_get_title();
  
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

?>