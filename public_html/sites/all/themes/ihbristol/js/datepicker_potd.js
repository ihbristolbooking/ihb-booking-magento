( function($) {   

 
 $(document).ready(function(){

 	$('.datepicker').datepicker({
 		dateFormat: 'dd/mm/yy', 
 		showOn: "both",
      	buttonImage: "/sites/all/themes/ihbristol/img/icon_calendar.gif",
      	buttonImageOnly: true,
      	buttonText: "Select date"
      });

	$('#potd_submit').click(function(){

		var view = $('#potd_search').attr('data-view');
		var date = $('#potd_search').children('input').val();

		if(date==null)
			return false;

		var dArray = date.split("/");

		window.location.href = '/'+view+'/'+dArray[2]+'-'+dArray[1]+'-'+dArray[0];

		return false;
	})

 })



})( jQuery );