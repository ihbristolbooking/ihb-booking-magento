(function($) { 

	$(document).ready(function(){

		$('span.file a').attr('target','_new').attr('title','Click to download file');

		$('.accordion-toggle').css('display','block');

		$(".fancybox").fancybox({

		    afterLoad: function() {

		    	var download_link = this.href;
		    	download_link  = download_link.replace('styles/large/public/','')

		        this.title = '<a download="'+this.title+'" href="' + download_link + '">Download</a> ' + this.title;
		    },
		    helpers : {
		        title: {
		            type: 'inside'
		        }
		    }
		});
	})

})(jQuery);