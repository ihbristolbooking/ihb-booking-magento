( function($) {   
 
 $(document).ready(function(){

  $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-full-board').parent('label').parent('div').hide();


  $('#edit-field-learn-english-course-type-und').on('change',function(){



  var thisVal = $("input[name='field_learn_english_course_type[und]']:checked").val();

  if(thisVal==46){
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-full-board').parent('label').parent('div').show(); 
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-half-board').parent('label').parent('div').hide();
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-self-board').parent('label').parent('div').hide();
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-residence').parent('label').parent('div').hide();

    } else {
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-full-board').parent('label').parent('div').hide(); 
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-half-board').parent('label').parent('div').show();
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-host-family-self-board').parent('label').parent('div').show();
      $('#edit-field-accommodation-type-und input#edit-field-accommodation-type-und-residence').parent('label').parent('div').show();
    }

  })



 	var c1 = $('#edit-field-learn-english-course-type-und-56').parent('label').parent('div');
 	var c2 = $('#edit-field-learn-english-course-type-und-71').parent('label').parent('div');
 	var c3 = $('#edit-field-learn-english-course-type-und-60').parent('label').parent('div');

	c1.after($('#edit-field-full-time-ielts-exam-cours'));
	c2.after($('#edit-field-cambridge-exam-course-type'));
	c3.after($('#edit-field-cambridge-exam-summer-cour'));


  $('.page-eform #edit-field-course-dates-und-0-value-datepicker-popup-0').ready(function(){
  
      var date = $('#edit-field-course-dates-und-0-value-datepicker-popup-0').val();

      var temp = new Array();

      if(date==undefined)
      return;

      temp = date.split("/");

      if(!temp[0])
        return;

      var dd = temp[0];
      var mm = temp[1];
      var yyyy = temp[2];



      checkDates(dd,mm,yyyy);
 })


 	$('.page-eform #edit-field-course-dates-und-0-value-datepicker-popup-0').on('change',function(){

      var date = $('#edit-field-course-dates-und-0-value-datepicker-popup-0').val();

      console.log(date);

      var temp = new Array();
      temp = date.split("/");

      var dd = temp[0];
      var mm = temp[1];
      var yyyy = temp[2];

     checkDates(dd,mm,yyyy);  

 	})

 })



  function checkDates(dd,mm,yyyy){

            var hideiflessthan = 14;

            var startDate = new Date(yyyy+"-"+mm+"-"+dd); 

            var today = new Date();
            var diff = (startDate - today)/(1000*60*60*24);

            if(diff < hideiflessthan){
                $("input#edit-field-process-und-no").parent('label').parent('div.form-item').hide()
            } else {
                // do nothing
                 $("input#edit-field-process-und-no").parent('label').parent('div.form-item').show()

            }
  }


})( jQuery );



