(function($) { 

	$(document).ready(function(){

		$('a[href="#top"]').on('click',function() {

		var target = $('[name=top]');

	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
		});

	  $('ul.anchor-links a[href*="#"]:not([href="#"])').on('click',function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	})
	
})(jQuery);