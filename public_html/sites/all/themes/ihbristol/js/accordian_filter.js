(function ($) {


  $('#block-views-other-languages-block .views-bootstrap-accordion-plugin-style .panel').ready(function(){

    $('#block-views-other-languages-block').find('.panel-body').each(function(){

      var lesson_format = $(this).find('.field-name-field-lesson-format').children('.field-items').children('.field-item').text();
      $(this).find('.field-name-field-lesson-format').remove();

      $(this).find('tbody').children('tr').each(function(){

        var thisLessonFormat = $(this).attr('data-lesson-format');
        if(thisLessonFormat != lesson_format){
          $(this).remove();
        }
      })

      $(this).find('tbody').each(function(){

        var hasContents = $(this).children().length;
        if(hasContents==0){ 
          $(this).prev('caption').remove();
          $(this).parent('table').remove();
        }
      })

      var l= $(this).find('.view-all-courses').children('.view-content').children().length;

      if(l==0)
        $(this).find('.field-name-magento-products').remove();

    })

  })

})(jQuery);