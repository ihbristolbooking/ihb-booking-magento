(function ($) {


    function enableMobileMenu(){

        var win = $( window ).width();

        //console.log(win);

        if(win < 770){

            $('body').addClass('mobile').addClass('mobile-portrait');

            $('div.region-sidebar-first section.block').each(function(){

                $(this).addClass('active');

                // hide the menu
                $(this).find('div.menu-block-wrapper').addClass('content-hide');
                $(this).find('div.view').addClass('content-hide');

                $(this).find('h2').on('click press',function(){

                    $(this).toggleClass('open');
                    $(this).next('div').toggleClass('content-hide');

                })
            })
        } else {
            $('body').removeClass('mobile').removeClass('mobile-portrait');
        }
    };

    $(document).ready(function () {

        enableMobileMenu();

        // check if we need
        $( window ).resize(function() {
            enableMobileMenu();
        });

        $('table tr th').each(function () {

            var selectClass = $(this).attr('class');
            var selectTitle = $(this).text();

            if(selectClass.length > 0) {

                selectClass = selectClass.replace('views-field ', '');
                $('.' + selectClass).attr('data-title', selectTitle);
            }

        })


        if ($("body").hasClass('front') && $("body").hasClass('logged-out')) {

            $(window).bind('resize', function (e) {

                if (window.RT) clearTimeout(window.RT);
                window.RT = setTimeout(function () {
                    this.location.reload(false);
                    /* false to get page from cache */
                }, 100);

            })
        }
        ;

    })

})(jQuery);