<?php
/**
 * @file
 * taxonomy_export.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function taxonomy_export_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Other',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '02a831ef-4e18-41fe-a726-5a9b86f06442',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'The Spark',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '1c12a1c2-7155-411a-9fac-f130cb6b02a1',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'London Heathrow (LHR)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '261c7952-1603-4d8c-82f9-b5d057c930cd',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Monday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2691ff9e-4483-4a60-a0b5-34eb63cde8f3',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Friend or Relative',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '299e6389-787a-4338-b260-9e3eaed10807',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'London Stanstead (STN)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3fa333cf-cc1d-43b3-a32d-a88e7c2e7e88',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Search Engine (e.g. Google)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '46570343-d399-40b6-98a3-fae482715b8a',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Bristol International Airport (BRI)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '4b25ffbc-e811-4e91-b29e-cfb09e1f2896',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '4c7e86a0-bbd1-44fb-8819-b393825bd42a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '507dc1df-690f-4296-a0fe-6e010ff34fdf',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '5b7d17f2-122d-4f31-ac9a-8ec145efdfe6',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '5c82fd07-3ec6-4511-9b17-fe0cbbe6152f',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Clifton Life Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '648a973a-1e53-422b-a7a2-3efc79c579cf',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '75b5ec6d-7eaf-4a9f-82da-e54f0fd3df7c',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Whats on Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '86342cb7-d21a-4fb1-9321-0192854abc91',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '8a602335-55b4-47b1-8891-afa6b47b5b3a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Event',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '8f236926-80b9-42c2-bdb2-299bc865bb9f',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Birmingham (BHX)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a97681ae-d586-4cf5-b3a3-0e88a1e86a70',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'London Gatwick (LGW)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ae94ccb7-a2bc-4ad1-a5ae-fd2263500dab',
    'vocabulary_machine_name' => 'airports',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Other',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 8,
    'uuid' => 'c719a96e-d0a7-466e-a27a-aa65d854cccf',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Monday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'e82e1a54-5612-45a9-8627-1d2aa12d1f3b',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Bristol Airport',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'e94788f0-6f38-439d-93c4-9d61806879b7',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Bristol Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'f27c6c96-b2a5-4c12-b7f1-270f0c2f7153',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
