<?php
/**
 * @file
 * learn_english_application.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function learn_english_application_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'taxonomy_term|language|default';
  $ds_fieldsetting->entity_type = 'taxonomy_term';
  $ds_fieldsetting->bundle = 'language';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['taxonomy_term|language|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function learn_english_application_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'entityform|english_application|default';
  $ds_layout->entity_type = 'entityform';
  $ds_layout->bundle = 'english_application';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_6_6';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'group_course',
        1 => 'group_your_details',
        3 => 'group_allergies',
        4 => 'group_accommodation_details',
        5 => 'field_first_name',
        9 => 'field_surname',
        10 => 'field_sex',
        13 => 'field_date_of_birth',
        18 => 'field_nationality',
        20 => 'field_passport_id_number',
        23 => 'field_uk_address',
        25 => 'field_accommodation_type',
        27 => 'field_accommodation_dates',
        29 => 'field_dietary_requirements',
        30 => 'field_learn_english_course_type',
        31 => 'field_dietary_other',
        32 => 'field_full_time_ielts_exam_cours',
        33 => 'field_smoking_options',
        34 => 'field_cambridge_exam_course_type',
        35 => 'field_dogs',
        36 => 'field_cambridge_exam_summer_cour',
        37 => 'field_course_dates',
        38 => 'field_cats',
        39 => 'field_allergies',
        40 => 'field_start_date',
        41 => 'field_allergies_details',
        42 => 'field_end_date',
        43 => 'field_duration',
        44 => 'field_agent_booking',
        45 => 'field_agency_name',
        46 => 'field_embassy_sponsor',
        47 => 'field_embassy_name',
      ),
      'right' => array(
        2 => 'field_address',
        6 => 'field_email',
        7 => 'group_travel',
        8 => 'field_telephone',
        11 => 'group_contact_details',
        12 => 'group_emergency_details',
        14 => 'group_info',
        15 => 'field_exams_taken',
        16 => 'field_emergency_name',
        17 => 'field_level',
        19 => 'field_lead_generation',
        21 => 'field_emergency_telephone',
        22 => 'field_marketing_other',
        24 => 'field_emergency_email',
        26 => 'field_payment_methods',
        28 => 'field_terms',
        48 => 'field_arrival_flight_details',
        49 => 'field_airport_arrive_airport',
        50 => 'field_airport_depart_other',
        51 => 'field_airport_arrive',
        52 => 'field_departure_flight_details',
        53 => 'field_airport_depart_airport',
        54 => 'field_airport_arrive_other',
        55 => 'field_airport_depart',
        56 => 'field_ih_contact',
      ),
    ),
    'fields' => array(
      'group_course' => 'left',
      'group_your_details' => 'left',
      'field_address' => 'right',
      'group_allergies' => 'left',
      'group_accommodation_details' => 'left',
      'field_first_name' => 'left',
      'field_email' => 'right',
      'group_travel' => 'right',
      'field_telephone' => 'right',
      'field_surname' => 'left',
      'field_sex' => 'left',
      'group_contact_details' => 'right',
      'group_emergency_details' => 'right',
      'field_date_of_birth' => 'left',
      'group_info' => 'right',
      'field_exams_taken' => 'right',
      'field_emergency_name' => 'right',
      'field_level' => 'right',
      'field_nationality' => 'left',
      'field_lead_generation' => 'right',
      'field_passport_id_number' => 'left',
      'field_emergency_telephone' => 'right',
      'field_marketing_other' => 'right',
      'field_uk_address' => 'left',
      'field_emergency_email' => 'right',
      'field_accommodation_type' => 'left',
      'field_payment_methods' => 'right',
      'field_accommodation_dates' => 'left',
      'field_terms' => 'right',
      'field_dietary_requirements' => 'left',
      'field_learn_english_course_type' => 'left',
      'field_dietary_other' => 'left',
      'field_full_time_ielts_exam_cours' => 'left',
      'field_smoking_options' => 'left',
      'field_cambridge_exam_course_type' => 'left',
      'field_dogs' => 'left',
      'field_cambridge_exam_summer_cour' => 'left',
      'field_course_dates' => 'left',
      'field_cats' => 'left',
      'field_allergies' => 'left',
      'field_start_date' => 'left',
      'field_allergies_details' => 'left',
      'field_end_date' => 'left',
      'field_duration' => 'left',
      'field_agent_booking' => 'left',
      'field_agency_name' => 'left',
      'field_embassy_sponsor' => 'left',
      'field_embassy_name' => 'left',
      'field_arrival_flight_details' => 'right',
      'field_airport_arrive_airport' => 'right',
      'field_airport_depart_other' => 'right',
      'field_airport_arrive' => 'right',
      'field_departure_flight_details' => 'right',
      'field_airport_depart_airport' => 'right',
      'field_airport_arrive_other' => 'right',
      'field_airport_depart' => 'right',
      'field_ih_contact' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['entityform|english_application|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'entityform|english_application|form';
  $ds_layout->entity_type = 'entityform';
  $ds_layout->bundle = 'english_application';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'group_course',
        1 => 'group_booking',
        2 => 'group_your_details',
        3 => 'field_first_name',
        4 => 'field_address',
        5 => 'group_emergency_contact',
        6 => 'field_surname',
        7 => 'field_sex',
        8 => 'group_level',
        9 => 'group_accommodation',
        10 => 'field_date_of_birth',
        11 => 'field_nationality',
        12 => 'group_diet',
        13 => 'group_smoking',
        14 => 'field_passport_id_number',
        15 => 'field_exams_taken',
        16 => 'group_pets',
        17 => 'field_email',
        18 => 'field_level',
        19 => 'field_telephone',
        20 => 'group_allergies',
        21 => 'group_travel',
        22 => 'field_emergency_name',
        23 => 'group_payment',
        24 => 'field_lead_generation',
        25 => 'field_allergies',
        26 => 'field_emergency_telephone',
        27 => 'field_allergies_details',
        28 => 'field_dietary_requirements',
        29 => 'field_marketing_other',
        30 => 'field_emergency_email',
        31 => 'field_smoking_options',
        32 => 'field_accommodation_type',
        33 => 'field_html_insurance',
        34 => 'field_terms',
        35 => 'field_uk_address',
        36 => 'field_dietary_other',
        37 => 'field_accommodation_dates',
        38 => 'field_learn_english_course_type',
        39 => 'field_full_time_ielts_exam_cours',
        40 => 'field_cambridge_exam_course_type',
        41 => 'field_cambridge_exam_summer_cour',
        42 => 'field_agent_booking',
        43 => 'field_agency_name',
        45 => 'field_course_dates',
        47 => 'field_duration',
        48 => 'field_embassy_sponsor',
        49 => 'field_dogs',
        50 => 'field_embassy_name',
        51 => 'field_cats',
        52 => 'field_arrival_flight_details',
        53 => 'field_airport_arrive',
        54 => 'field_airport_arrive_airport',
        55 => 'field_airport_arrive_other',
        56 => 'field_departure_flight_details',
        57 => 'field_airport_depart',
        58 => 'field_airport_depart_airport',
        59 => 'field_payment_methods',
        60 => 'field_airport_depart_other',
        61 => 'field_ih_contact',
      ),
      'hidden' => array(
        44 => 'field_start_date',
        46 => 'field_end_date',
        62 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'group_course' => 'central',
      'group_booking' => 'central',
      'group_your_details' => 'central',
      'field_first_name' => 'central',
      'field_address' => 'central',
      'group_emergency_contact' => 'central',
      'field_surname' => 'central',
      'field_sex' => 'central',
      'group_level' => 'central',
      'group_accommodation' => 'central',
      'field_date_of_birth' => 'central',
      'field_nationality' => 'central',
      'group_diet' => 'central',
      'group_smoking' => 'central',
      'field_passport_id_number' => 'central',
      'field_exams_taken' => 'central',
      'group_pets' => 'central',
      'field_email' => 'central',
      'field_level' => 'central',
      'field_telephone' => 'central',
      'group_allergies' => 'central',
      'group_travel' => 'central',
      'field_emergency_name' => 'central',
      'group_payment' => 'central',
      'field_lead_generation' => 'central',
      'field_allergies' => 'central',
      'field_emergency_telephone' => 'central',
      'field_allergies_details' => 'central',
      'field_dietary_requirements' => 'central',
      'field_marketing_other' => 'central',
      'field_emergency_email' => 'central',
      'field_smoking_options' => 'central',
      'field_accommodation_type' => 'central',
      'field_html_insurance' => 'central',
      'field_terms' => 'central',
      'field_uk_address' => 'central',
      'field_dietary_other' => 'central',
      'field_accommodation_dates' => 'central',
      'field_learn_english_course_type' => 'central',
      'field_full_time_ielts_exam_cours' => 'central',
      'field_cambridge_exam_course_type' => 'central',
      'field_cambridge_exam_summer_cour' => 'central',
      'field_agent_booking' => 'central',
      'field_agency_name' => 'central',
      'field_start_date' => 'hidden',
      'field_course_dates' => 'central',
      'field_end_date' => 'hidden',
      'field_duration' => 'central',
      'field_embassy_sponsor' => 'central',
      'field_dogs' => 'central',
      'field_embassy_name' => 'central',
      'field_cats' => 'central',
      'field_arrival_flight_details' => 'central',
      'field_airport_arrive' => 'central',
      'field_airport_arrive_airport' => 'central',
      'field_airport_arrive_other' => 'central',
      'field_departure_flight_details' => 'central',
      'field_airport_depart' => 'central',
      'field_airport_depart_airport' => 'central',
      'field_payment_methods' => 'central',
      'field_airport_depart_other' => 'central',
      'field_ih_contact' => 'central',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['entityform|english_application|form'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function learn_english_application_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'listing';
  $ds_view_mode->label = 'listing';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['listing'] = $ds_view_mode;

  return $export;
}
