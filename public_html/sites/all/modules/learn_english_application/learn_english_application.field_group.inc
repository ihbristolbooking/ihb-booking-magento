<?php
/**
 * @file
 * learn_english_application.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function learn_english_application_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_accommodation_details|entityform|english_application|default';
  $field_group->group_name = 'group_accommodation_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Accommodation Details',
    'weight' => '3',
    'children' => array(
      0 => 'field_uk_address',
      1 => 'field_accommodation_type',
      2 => 'field_accommodation_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Accommodation Details',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-accommodation-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_accommodation_details|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_accommodation|entityform|english_application|form';
  $field_group->group_name = 'group_accommodation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Accommodation',
    'weight' => '6',
    'children' => array(
      0 => 'field_uk_address',
      1 => 'field_accommodation_type',
      2 => 'field_accommodation_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Accommodation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-accommodation field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_accommodation|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_allergies|entityform|english_application|default';
  $field_group->group_name = 'group_allergies';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Allergies',
    'weight' => '2',
    'children' => array(
      0 => 'field_dietary_requirements',
      1 => 'field_dietary_other',
      2 => 'field_smoking_options',
      3 => 'field_dogs',
      4 => 'field_cats',
      5 => 'field_allergies',
      6 => 'field_allergies_details',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-allergies field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_allergies|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_allergies|entityform|english_application|form';
  $field_group->group_name = 'group_allergies';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Allergies / Medical / Special Educational Needs',
    'weight' => '10',
    'children' => array(
      0 => 'field_allergies',
      1 => 'field_allergies_details',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Allergies / Medical / Special Educational Needs',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-allergies field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_allergies|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_booking|entityform|english_application|form';
  $field_group->group_name = 'group_booking';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Booking Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_agent_booking',
      1 => 'field_agency_name',
      2 => 'field_duration',
      3 => 'field_embassy_sponsor',
      4 => 'field_embassy_name',
      5 => 'field_course_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Booking Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-booking field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_booking|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact_details|entityform|english_application|default';
  $field_group->group_name = 'group_contact_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact Details',
    'weight' => '5',
    'children' => array(
      0 => 'field_email',
      1 => 'field_telephone',
      2 => 'field_address',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Contact Details',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-contact-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_contact_details|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact_details|entityform|english_application|form';
  $field_group->group_name = 'group_contact_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact Details',
    'weight' => '2',
    'children' => array(
      0 => 'field_address',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-contact-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_contact_details|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course|entityform|english_application|default';
  $field_group->group_name = 'group_course';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your Course',
    'weight' => '0',
    'children' => array(
      0 => 'field_full_time_ielts_exam_cours',
      1 => 'field_cambridge_exam_course_type',
      2 => 'field_cambridge_exam_summer_cour',
      3 => 'field_learn_english_course_type',
      4 => 'field_agent_booking',
      5 => 'field_agency_name',
      6 => 'field_start_date',
      7 => 'field_end_date',
      8 => 'field_duration',
      9 => 'field_embassy_sponsor',
      10 => 'field_embassy_name',
      11 => 'field_course_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-course field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_course|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course|entityform|english_application|form';
  $field_group->group_name = 'group_course';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your Course',
    'weight' => '0',
    'children' => array(
      0 => 'field_full_time_ielts_exam_cours',
      1 => 'field_cambridge_exam_course_type',
      2 => 'field_cambridge_exam_summer_cour',
      3 => 'field_learn_english_course_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your Course',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-course field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_course|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_diet|entityform|english_application|form';
  $field_group->group_name = 'group_diet';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Dietary Requirements',
    'weight' => '7',
    'children' => array(
      0 => 'field_dietary_requirements',
      1 => 'field_dietary_other',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Dietary Requirements',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-diet field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_diet|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_emergency_contact|entityform|english_application|form';
  $field_group->group_name = 'group_emergency_contact';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Emergency Contact',
    'weight' => '4',
    'children' => array(
      0 => 'field_emergency_name',
      1 => 'field_emergency_telephone',
      2 => 'field_emergency_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Emergency Contact',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-emergency-contact field-group-fieldset',
        'description' => ' For students under 18, this should be your parent/guardian.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_emergency_contact|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_emergency_details|entityform|english_application|default';
  $field_group->group_name = 'group_emergency_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Emergency Contact',
    'weight' => '6',
    'children' => array(
      0 => 'field_emergency_name',
      1 => 'field_emergency_telephone',
      2 => 'field_emergency_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-emergency-details field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_emergency_details|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|entityform|english_application|default';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Info',
    'weight' => '7',
    'children' => array(
      0 => 'field_exams_taken',
      1 => 'field_level',
      2 => 'field_marketing_other',
      3 => 'field_lead_generation',
      4 => 'field_terms',
      5 => 'field_payment_methods',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-info field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_info|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_level|entityform|english_application|form';
  $field_group->group_name = 'group_level';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your level of English',
    'weight' => '5',
    'children' => array(
      0 => 'field_exams_taken',
      1 => 'field_level',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your level of English',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-level field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_level|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_payment|entityform|english_application|form';
  $field_group->group_name = 'group_payment';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Method',
    'weight' => '12',
    'children' => array(
      0 => 'field_payment_methods',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Payment Method',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-payment field-group-fieldset',
        'description' => 'Once we have checked availability, we will send you an invoice.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_payment|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pets|entityform|english_application|form';
  $field_group->group_name = 'group_pets';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pets',
    'weight' => '9',
    'children' => array(
      0 => 'field_dogs',
      1 => 'field_cats',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Pets',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-pets field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_pets|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_smoking|entityform|english_application|form';
  $field_group->group_name = 'group_smoking';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Smoking',
    'weight' => '8',
    'children' => array(
      0 => 'field_smoking_options',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Smoking',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-smoking field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_smoking|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_travel|entityform|english_application|default';
  $field_group->group_name = 'group_travel';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Travel Arrangements',
    'weight' => '4',
    'children' => array(
      0 => 'field_airport_depart',
      1 => 'field_airport_depart_other',
      2 => 'field_airport_arrive_other',
      3 => 'field_airport_depart_airport',
      4 => 'field_airport_arrive_airport',
      5 => 'field_airport_arrive',
      6 => 'field_arrival_flight_details',
      7 => 'field_departure_flight_details',
      8 => 'field_ih_contact',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-travel field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_travel|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_travel|entityform|english_application|form';
  $field_group->group_name = 'group_travel';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Travel Arrangements',
    'weight' => '11',
    'children' => array(
      0 => 'field_airport_depart',
      1 => 'field_airport_depart_other',
      2 => 'field_airport_arrive_other',
      3 => 'field_airport_depart_airport',
      4 => 'field_airport_arrive_airport',
      5 => 'field_airport_arrive',
      6 => 'field_arrival_flight_details',
      7 => 'field_departure_flight_details',
      8 => 'field_ih_contact',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Travel Arrangements',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-travel field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_travel|entityform|english_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_your_details|entityform|english_application|default';
  $field_group->group_name = 'group_your_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_first_name',
      1 => 'field_surname',
      2 => 'field_sex',
      3 => 'field_date_of_birth',
      4 => 'field_nationality',
      5 => 'field_passport_id_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your Details',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-your-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_your_details|entityform|english_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_your_details|entityform|english_application|form';
  $field_group->group_name = 'group_your_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'english_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your Details',
    'weight' => '2',
    'children' => array(
      0 => 'field_first_name',
      1 => 'field_surname',
      2 => 'field_sex',
      3 => 'field_date_of_birth',
      4 => 'field_nationality',
      5 => 'field_passport_id_number',
      6 => 'field_email',
      7 => 'field_telephone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-your-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_your_details|entityform|english_application|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Accommodation');
  t('Accommodation Details');
  t('Allergies');
  t('Allergies / Medical / Special Educational Needs');
  t('Booking Details');
  t('Contact Details');
  t('Dietary Requirements');
  t('Emergency Contact');
  t('Other Info');
  t('Payment Method');
  t('Pets');
  t('Smoking');
  t('Travel Arrangements');
  t('Your Course');
  t('Your Details');
  t('Your level of English');

  return $field_groups;
}
