<?php

/**
 * @file
 * Locale Currency module
 * Allows admins to define a currency code for each of their locales.
 *
 * @author Tom Kirkpatrick (mrfelton), www.systemseed.com
 */

/**
 * Implements hook_form_alter().
 */
function locale_currency_form_alter(&$form, &$form_state, $form_id) {
  if (($form_id == 'locale_languages_edit_form') || ($form_id == 'locale_languages_custom_form')) {
    $languages = language_list();
    $field = array(
      '#type' => 'textfield',
      '#title' => 'Currency Code',
      '#maxlength' => 3,
      '#size' => 12,
      '#default_value' => isset($languages[$form['langcode']['#value']]->currency) ? $languages[$form['langcode']['#value']]->currency : '',
      '#description' => t('<a href="@link">ISO 4217</a> compliant currency identifier. Examples: "GBP", "EUR", and "USD".', array('@link' => 'http://www.oanda.com/site/help/iso_code.shtml')),
    );
    $form['#submit'][] = 'locale_currency_edit_submit';
    $form['submit']['#weight'] = 10;
    
    switch ($form_id) {
      case 'locale_languages_edit_form':
        $form['currency'] = $field;
        break;

      case 'locale_languages_custom_form':
        $form['custom language']['currency'] = $field;
        break;
    }
  }
}

/**
 * Custom submit handler for the locale language form.
 */
function locale_currency_edit_submit($form, &$form_state) {
  dsm($form_state);
  db_update('languages')
  ->fields(array(
    'currency' => $form_state['values']['currency'],
  ))
  ->condition('language', $form_state['values']['langcode'])
  ->execute();
  $language_default = language_default();
  if ($language_default->language == $form_state['values']['langcode']) {
    $language_default->currency = $form_state['values']['currency'];
    variable_set('language_default', $language_default);
  }
}

/**
 * API function to get the currency code associated with a specific locale.
 *
 * @param $lang
 *   Language code whose associated currency to look up.
 *
 * @return
 *   Currency associated with requested language, or NULL if there was none.
 */
function locale_currency_get_currency($lang) {
  $languages = language_list('enabled');
  $language = $languages[1][$lang];
  if (isset($language)) {
    return $language->currency;
  }
  return NULL;
}

/**
 * API function to get the currency code associated with current language.
 *
 * @return
 *   Currency code associated with the current language.
 */
function locale_currency_get_current_currency() {
  global $language;
  return $language->currency;
}
