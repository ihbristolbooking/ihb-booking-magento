<?php
/**
 * @file
 * teacher_training_application.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function teacher_training_application_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'The Spark',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '1c12a1c2-7155-411a-9fac-f130cb6b02a1',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Monday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2691ff9e-4483-4a60-a0b5-34eb63cde8f3',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Friend or Relative',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '299e6389-787a-4338-b260-9e3eaed10807',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'German',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3b001d26-5a46-4837-92e5-ac69fa276cb8',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Saturday 10:45 - 11:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '412553d5-a2c1-4fca-9d93-d5318be74495',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Search Engine (e.g. Google)',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '46570343-d399-40b6-98a3-fae482715b8a',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '4c7e86a0-bbd1-44fb-8819-b393825bd42a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '507dc1df-690f-4296-a0fe-6e010ff34fdf',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'French',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5a35b89e-41fa-4677-853f-57abbb7155ba',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '5b7d17f2-122d-4f31-ac9a-8ec145efdfe6',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '5c82fd07-3ec6-4511-9b17-fe0cbbe6152f',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Clifton Life Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '648a973a-1e53-422b-a7a2-3efc79c579cf',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Mandarin',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '68f6a3aa-a521-4a13-8b4c-f351c16d98b4',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Japanese',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '6a3fdc19-0dca-4cbc-9af3-a1fe52a113fb',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '75b5ec6d-7eaf-4a9f-82da-e54f0fd3df7c',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Italian',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '76b63bb0-7cc9-497d-883f-1bc0e7c572c1',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Whats on Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '86342cb7-d21a-4fb1-9321-0192854abc91',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '8a602335-55b4-47b1-8891-afa6b47b5b3a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Event',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '8f236926-80b9-42c2-bdb2-299bc865bb9f',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Other',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 8,
    'uuid' => 'c719a96e-d0a7-466e-a27a-aa65d854cccf',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'English',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'd99aaeef-6172-48cb-b50e-ac5ed8e9c79a',
    'vocabulary_machine_name' => 'language',
  );
  $terms[] = array(
    'name' => 'Monday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'e82e1a54-5612-45a9-8627-1d2aa12d1f3b',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Bristol Airport',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'e94788f0-6f38-439d-93c4-9d61806879b7',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Bristol Magazine',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'f27c6c96-b2a5-4c12-b7f1-270f0c2f7153',
    'vocabulary_machine_name' => 'lead_generation',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
