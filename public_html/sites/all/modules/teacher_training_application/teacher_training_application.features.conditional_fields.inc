<?php
/**
 * @file
 * teacher_training_application.features.conditional_fields.inc
 */

/**
 * Implements hook_conditional_fields_default_fields().
 */
function teacher_training_application_conditional_fields_default_fields() {
  $items = array();

  $items["entityform:teacher_training_application:0"] = array(
    'entity' => 'entityform',
    'bundle' => 'teacher_training_application',
    'dependent' => 'field_marketing_other',
    'dependee' => 'field_lead_generation',
    'options' => array(
      'state' => 'visible',
      'condition' => 'value',
      'grouping' => 'AND',
      'effect' => 'show',
      'effect_options' => array(),
      'element_view' => array(
        1 => 1,
        2 => 2,
        5 => 0,
        3 => 0,
        4 => 0,
      ),
      'element_view_per_role' => 0,
      'element_view_roles' => array(
        1 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        2 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        3 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
      ),
      'element_edit' => array(
        1 => 1,
        3 => 0,
      ),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(
        1 => array(
          1 => 1,
          3 => 0,
        ),
        2 => array(
          1 => 1,
          3 => 0,
        ),
        3 => array(
          1 => 1,
          3 => 0,
        ),
      ),
      'selector' => '',
      'values_set' => 1,
      'value_form' => 43,
      'value' => array(
        0 => array(
          'target_id' => 43,
        ),
      ),
      'values' => array(),
    ),
  );

  return $items;
}
