<?php
/**
 * @file
 * teacher_training_application.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function teacher_training_application_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_applicant|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_applicant';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Applicant Information',
    'weight' => '5',
    'children' => array(
      0 => 'field_sex',
      1 => 'field_date_of_birth',
      2 => 'field_nationality',
      3 => 'field_telephone',
      4 => 'field_mobile_telephone',
      5 => 'field_email',
      6 => 'field_address',
      7 => 'field_applicant',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Applicant Information',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-applicant field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_applicant|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_celta|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_celta';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'CELTA Questions',
    'weight' => '5',
    'children' => array(
      0 => 'field_dyslexia',
      1 => 'field_special_conditions',
      2 => 'field_needs_and_requirements',
      3 => 'field_offences',
      4 => 'field_special_more_info',
      5 => 'group_work_experience',
      6 => 'group_languages',
      7 => 'group_education',
      8 => 'group_teaching_experience',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'CELTA Questions',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-celta field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_celta|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_course';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Course',
    'weight' => '0',
    'children' => array(
      0 => 'field_tt_course_date',
      1 => 'field_alternative_course_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Course',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-course field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_course|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_education|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_education';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Education',
    'weight' => '3',
    'children' => array(
      0 => 'field_secondary_education',
      1 => 'field_higher_education',
      2 => 'field_professional_training',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-education field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_education|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_education|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_education';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_celta';
  $field_group->data = array(
    'label' => 'Education',
    'weight' => '12',
    'children' => array(
      0 => 'field_secondary_education',
      1 => 'field_higher_education',
      2 => 'field_professional_training',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Education',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-education field-group-fieldset',
        'description' => 'Please give details of secondary, higher and further education and professional training, including dates, subjects studied and qualifications gained',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_education|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_emergency_contact|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_emergency_contact';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Emergency Contact',
    'weight' => '7',
    'children' => array(
      0 => 'field_emergency_name',
      1 => 'field_emergency_telephone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-emergency-contact field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_emergency_contact|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_emergency_contact|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_emergency_contact';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Emergency Contact Details',
    'weight' => '3',
    'children' => array(
      0 => 'field_emergency_name',
      1 => 'field_emergency_telephone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Emergency Contact Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-emergency-contact field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_emergency_contact|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extra|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_extra';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Extra Info',
    'weight' => '4',
    'children' => array(
      0 => 'field_lead_generation',
      1 => 'field_marketing_other',
      2 => 'field_occupation',
      3 => 'field_course_how',
      4 => 'field_accommodation_help',
      5 => 'field_dyslexia',
      6 => 'field_special_conditions',
      7 => 'field_needs_and_requirements',
      8 => 'field_offences',
      9 => 'field_special_more_info',
      10 => 'field_extra_info',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-extra field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_extra|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_further_info|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_further_info';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Further Information',
    'weight' => '4',
    'children' => array(
      0 => 'field_occupation',
      1 => 'field_alternative_course_date',
      2 => 'field_course_how',
      3 => 'field_accommodation_help',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Further Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-further-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_further_info|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_languages_known|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_languages_known';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Languages Known',
    'weight' => '8',
    'children' => array(
      0 => 'field_languages_known',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-languages-known field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_languages_known|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_languages|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_languages';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_celta';
  $field_group->data = array(
    'label' => 'Languages',
    'weight' => '14',
    'children' => array(
      0 => 'field_languages_known',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Languages',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-languages field-group-fieldset',
        'description' => 'Apart from your native language, do you know any other languages, and to what level approximately? e.g. elementary, intermediate, advanced.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_languages|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_language|multifield|field_languages_known|default';
  $field_group->group_name = 'group_language';
  $field_group->entity_type = 'multifield';
  $field_group->bundle = 'field_languages_known';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Language',
    'weight' => '0',
    'children' => array(
      0 => 'field_mf_language',
      1 => 'field_speaking_listening',
      2 => 'field_writing',
      3 => 'field_reading',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-language field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_language|multifield|field_languages_known|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching_experience|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_teaching_experience';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_celta';
  $field_group->data = array(
    'label' => 'Teaching Experience',
    'weight' => '13',
    'children' => array(
      0 => 'field_experience_efl',
      1 => 'field_experience_other_subject',
      2 => 'field_experience_teacher_trainin',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Teaching Experience',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-teaching-experience field-group-fieldset',
        'description' => 'Do you have any experience of teaching or training? If yes, please give full details.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_teaching_experience|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_training';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Training',
    'weight' => '2',
    'children' => array(
      0 => 'field_experience_efl',
      1 => 'field_experience_other_subject',
      2 => 'field_experience_teacher_trainin',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_training|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_work_experience|entityform|teacher_training_application|default';
  $field_group->group_name = 'group_work_experience';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Work Experience',
    'weight' => '6',
    'children' => array(
      0 => 'field_work_experience',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-work-experience field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_work_experience|entityform|teacher_training_application|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_work_experience|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_work_experience';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_celta';
  $field_group->data = array(
    'label' => 'Work Experience',
    'weight' => '11',
    'children' => array(
      0 => 'field_work_experience',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Work Experience',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-work-experience field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_work_experience|entityform|teacher_training_application|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_your_details|entityform|teacher_training_application|form';
  $field_group->group_name = 'group_your_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'teacher_training_application';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_sex',
      1 => 'field_date_of_birth',
      2 => 'field_nationality',
      3 => 'field_telephone',
      4 => 'field_mobile_telephone',
      5 => 'field_email',
      6 => 'field_applicant',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Personal Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-your-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_your_details|entityform|teacher_training_application|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Applicant Information');
  t('CELTA Questions');
  t('Course');
  t('Education');
  t('Emergency Contact');
  t('Emergency Contact Details');
  t('Extra Info');
  t('Further Information');
  t('Language');
  t('Languages');
  t('Languages Known');
  t('Personal Details');
  t('Teaching Experience');
  t('Training');
  t('Work Experience');

  return $field_groups;
}
