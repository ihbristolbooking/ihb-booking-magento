<?php

/**
 * @file
 * Bulk export of ds objects generated by Bulk export module.
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function ds_export_ds_field_settings_info() {
  $ds_fieldsettings = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|useful_phrases-page_1|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'useful_phrases-page_1';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'header' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_advanced' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_elementary' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_intermediate' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $ds_fieldsettings['ds_views|useful_phrases-page_1|default'] = $ds_fieldsetting;

  return $ds_fieldsettings;
}
/**
 * Implements hook_ds_layout_settings_info().
 */
function ds_export_ds_layout_settings_info() {
  $ds_layouts = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|useful_phrases-page_1|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'useful_phrases-page_1';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_4_4_4_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'header',
      ),
      'left' => array(
        1 => 'useful_phrases_elementary',
      ),
      'central' => array(
        2 => 'useful_phrases_intermediate',
      ),
      'right' => array(
        3 => 'useful_phrases_advanced',
      ),
    ),
    'fields' => array(
      'header' => 'top',
      'useful_phrases_elementary' => 'left',
      'useful_phrases_intermediate' => 'central',
      'useful_phrases_advanced' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'central' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $ds_layouts['ds_views|useful_phrases-page_1|default'] = $ds_layout;

  return $ds_layouts;
}
/**
 * Implements hook_ds_view_modes_info().
 */
function ds_export_ds_view_modes_info() {
  $ds_view_modes = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'listing';
  $ds_view_mode->label = 'listing';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $ds_view_modes['listing'] = $ds_view_mode;

  return $ds_view_modes;
}
/**
 * Implements hook_ds_custom_fields_info().
 */
function ds_export_ds_custom_fields_info() {
  $ds_fields = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_advanced';
  $ds_field->label = 'Useful Phrases Advanced';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_3',
    'block_render' => '1',
  );
  $ds_fields['useful_phrases_advanced'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_elementary';
  $ds_field->label = 'Useful Phrases Elementary';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_1',
    'block_render' => '1',
  );
  $ds_fields['useful_phrases_elementary'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_intermediate';
  $ds_field->label = 'Useful Phrases Intermediate';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_2',
    'block_render' => '1',
  );
  $ds_fields['useful_phrases_intermediate'] = $ds_field;

  return $ds_fields;
}
