<?php
/**
 * @file
 * Handles the layout of the keyword_transformation answering form.
 *
 *
 * Variables available:
 * - $form
 */
print drupal_render($form);

?>