<?php
/**
 * @file
 * quiz_page_importer.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function quiz_page_importer_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'keyword_transformation';
  $feeds_importer->config = array(
    'name' => 'Keyword Transformation',
    'description' => 'Importer for keyword transformation question. Evaluation code values: 3 – manual, 4- custom scoring',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'keyword_transformation',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'row_text',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'answer',
            'target' => 'correct_answer',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'op_word',
            'target' => 'operative_word',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'op_sentence',
            'target' => 'operative_sentence',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'evaluation code',
            'target' => 'correct_answer_evaluation',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'question_id',
            'target' => 'field_question_id',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 1,
        'skip_hash_check' => 0,
        'update_non_existent' => 'skip',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['keyword_transformation'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'quiz';
  $feeds_importer->config = array(
    'name' => 'Quiz',
    'description' => 'Importer for Quiz',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'entry_id',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'url_title',
            'target' => 'url',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Introduction',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Type',
            'target' => 'field_quiz_category',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'mins',
            'target' => 'field_time_mins',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'secs',
            'target' => 'field_time_secs',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Available_when_not_logged_in',
            'target' => 'field_not_logged_in',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'section',
            'target' => 'field_quiz_type',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          11 => array(
            'source' => 'category',
            'target' => 'field_quiz_exam_course_type:label',
            'unique' => FALSE,
          ),
           12 => array(
            'source' => 'quiz_level',
            'target' => 'field_quiz_quiz_level',
            'unique' => FALSE,
          )
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'quiz',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['quiz'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'word_formation';
  $feeds_importer->config = array(
    'name' => 'Word Formation',
    'description' => 'Importer for word formation question. Evaluation code values: 0 – automatic & case sensitive, 1- automatic & case insensitive, 2 - regex, 3 – manual',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'word_formation',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'question',
            'target' => 'body',
            'unique' => FALSE,
            'format' => 'filtered_html'
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'answer',
            'target' => 'correct_answer',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'evaluation code',
            'target' => 'correct_answer_evaluation',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'question_id',
            'target' => 'field_question_id',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'op_word',
            'target' => 'operative_word',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 1,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['word_formation'] = $feeds_importer;



  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'multichoice';
  $feeds_importer->config = array(
  'name' => 'Multichoice',
  'description' => 'Importer for multichoice questions. See http://example.com/admin/quiz/import to set default values.',
  'fetcher' => array(
    'plugin_key' => 'FeedsFileFetcher',
    'config' => array(
      'direct' => FALSE,
      'allowed_extensions' => 'txt csv tsv xml opml',
      'directory' => 'public://feeds',
      'allowed_schemes' => array(
        0 => 'public',
      ),
    ),
  ),
  'parser' => array(
    'plugin_key' => 'FeedsCSVParser',
    'config' => array(
      'delimiter' => ',',
      'no_headers' => 0,
    ),
  ),
  'processor' => array(
    'plugin_key' => 'FeedsNodeProcessor',
    'config' => array(
      'bundle' => 'multichoice',
      'update_existing' => '1',
      'expire' => '-1',
      'mappings' => array(
        0 => array(
          'source' => 'guid',
          'target' => 'guid',
          'unique' => 1,
        ),
        1 => array(
          'source' => 'title',
          'target' => 'title',
          'unique' => FALSE,
        ),
        2 => array(
          'source' => 'question',
          'target' => 'body',
          'unique' => FALSE,
        ),
        3 => array(
          'source' => 'correct answer',
          'target' => 'multichoice_alternative_correct',
          'unique' => FALSE,
        ),
        4 => array(
          'source' => 'wrong answer 1',
          'target' => 'multichoice_alternative_incorrect',
          'unique' => FALSE,
        ),
        5 => array(
          'source' => 'wrong answer 2',
          'target' => 'multichoice_alternative_incorrect',
          'unique' => FALSE,
        ),
        6 => array(
          'source' => 'wrong answer 3',
          'target' => 'multichoice_alternative_incorrect',
          'unique' => FALSE,
        ),
        7 => array(
          'source' => 'wrong answer 4',
          'target' => 'multichoice_alternative_incorrect',
          'unique' => FALSE,
        ),
        8 => array(
          'source' => 'wrong answer 5',
          'target' => 'multichoice_alternative_incorrect',
          'unique' => FALSE,
        ),
        9 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
        10 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
        11 => array(
            'source' => 'question_id',
            'target' => 'field_question_id',
            'unique' => FALSE,
          ),
      ),
      'input_format' => 'plain_text',
      'author' => 1,
      'authorize' => 1,
      'skip_hash_check' => 0,
    ),
  ),
  'content_type' => '',
  'update' => 0,
  'import_period' => '-1',
  'expire_period' => 3600,
  'import_on_create' => 1,
  'process_in_background' => FALSE,
  );

  $export['multichoice'] = $feeds_importer;




  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'short_answer';
  $feeds_importer->config = array(
    'name' => 'Short Answer',
    'description' => 'Importer for short answer question. Evaluation code values: 0 – automatic & case sensitive, 1- automatic & case insensitive, 2 - regex, 3 – manual',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'short_answer',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'question',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'answer',
            'target' => 'short_answer_correct_answer',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'evaluation code',
            'target' => 'short_answer_evaluation',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'question_id',
            'target' => 'field_question_id',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 1,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['short_answer'] = $feeds_importer;



  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'gapped_sentence';
  $feeds_importer->config = array(
    'name' => ' Gapped Sentence',
    'description' => 'Importer for gapped sentence question. Evaluation code values: 0 – automatic & case sensitive, 1- automatic & case insensitive, 2 - regex, 3 – manual',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'gapped_sentence',
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'question',
            'target' => 'body',
            'unique' => FALSE,
            'format' => 'filtered_html',
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'answer',
            'target' => 'correct_answer',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'evaluation code',
            'target' => 'correct_answer_evaluation',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'entry_id',
            'target' => 'field_entry_id',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'question_id',
            'target' => 'field_question_id',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 1,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => FALSE,
  );
  $export['gapped_sentence'] = $feeds_importer;




$feeds_importer = new stdClass();
$feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
$feeds_importer->api_version = 1;
$feeds_importer->id = 'quiz_page';
$feeds_importer->config = array(
  'name' => 'Quiz Page',
  'description' => '',
  'fetcher' => array(
    'plugin_key' => 'FeedsFileFetcher',
    'config' => array(
      'allowed_extensions' => 'txt csv tsv xml opml',
      'direct' => FALSE,
      'directory' => 'public://feeds',
      'allowed_schemes' => array(
        0 => 'public',
      ),
    ),
  ),
  'parser' => array(
    'plugin_key' => 'FeedsCSVParser',
    'config' => array(
      'delimiter' => '|',
      'encoding' => 'UTF-8',
      'no_headers' => 0,
    ),
  ),
  'processor' => array(
    'plugin_key' => 'FeedsNodeProcessor',
    'config' => array(
      'expire' => '-1',
      'author' => 0,
      'authorize' => 1,
      'mappings' => array(
        0 => array(
          'source' => 'Title',
          'target' => 'title',
          'unique' => 1,
          'language' => 'und',
        ),
        1 => array(
          'source' => 'example',
          'target' => 'field_example',
            'unique' => FALSE,
        ),
        2 => array(
            'source' => 'iLearn',
            'target' => 'field_ilearn',
            'unique' => FALSE,
        ),
        3 => array(
            'source' => 'question0',
            'target' => 'field_question_0',
            'unique' => FALSE,
        ),
      ),
      'insert_new' => '1',
      'update_existing' => '2',
      'update_non_existent' => 'skip',
      'input_format' => 'plain_text',
      'skip_hash_check' => 0,
      'bundle' => 'quiz_page',
      'language' => 'en',
    ),
  ),
  'content_type' => '',
  'update' => 0,
  'import_period' => 1800,
  'expire_period' => 3600,
  'import_on_create' => TRUE,
  'process_in_background' => FALSE,
);


  $export['quiz_page'] = $feeds_importer;


  return $export;
}
