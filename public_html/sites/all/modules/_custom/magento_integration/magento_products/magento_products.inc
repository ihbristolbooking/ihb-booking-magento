<?php

/**
 * @file
 * Helper functions for the Drupal Magento Product integration module.
 */


  function _get_tid_from_term_name($term_name,$vocabulary) {
    $arr_terms = taxonomy_get_term_by_name($term_name, $vocabulary);
    if (!empty($arr_terms)) {
      $arr_terms = array_values($arr_terms);
      $tid = $arr_terms[0]->tid;
    }
     else {
      $vobj = taxonomy_vocabulary_machine_name_load($vocabulary);
      $term = new stdClass();
      $term->name = $term_name;
      $term->vid = $vobj->vid;
      taxonomy_term_save($term);
      $tid = $term->tid;
    }
    return $tid;
  }






/**
 * Import updated products into Magento.
 *
 * @return int
 *  The number of Magento products imported.
 */
function magento_products_import() {
  module_load_include('class.inc', 'magento_products');

  $magento_connection = new MagentoConnection();
  $magento = new MagentoProducts($magento_connection);

  // Find the a set of changed products since our last sync to update.

  // MJ IHB 3/7/17
  // Magento seems to add an hour to the sync time, presumably something to do with daylight saving
  // subtract an hour to compensate for this

  //$magento_date_last_sync = date('Y-m-d H:i:s', variable_get('magento_product_last_sync', 0));

  $ls = variable_get('magento_product_last_sync', 0);
  if($ls > 0) {
    $ls = $ls - 3600;
  }
  $magento_date_last_sync = date('Y-m-d H:i:s', $ls);

  $products_to_update = $magento->getProductsUpdatedAfter($magento_date_last_sync);


  $product_ids_to_import = array();

  if (!empty($products_to_update)) {
    foreach ($products_to_update as $product) {
      $values = array(
        ':magento_product_id' => $product['product_id'],
        ':created' => time(),
      );
      db_query('INSERT INTO {magento_products_queue} (magento_product_id, created) VALUES (:magento_product_id, :created) ON DUPLICATE KEY UPDATE magento_product_id = magento_product_id', $values);
    }

    // Track our last import time so that next time we run we will grab only newly
    // updated products into our queue.

    variable_set('magento_product_last_sync', time());

  }
  else {
    $message = 'No new products in Magento have updated since @time.';
    $variables = array('@time' => $magento_date_last_sync);
    watchdog('magento_products', $message, $variables);
  }

  // Now that the queue is updated we work down the queue in chunks.
  return magento_products_process_queue();
}

/**
 * Process queued imports in Magento.
 */
function magento_products_process_queue() {
  module_load_include('class.inc', 'magento_products');

  $magento_connection = new MagentoConnection();
  $magento = new MagentoProducts($magento_connection);

  $product_ids_to_import = array();
  $limit = variable_get('magento_products_import_limit', 50);
  $result = db_query_range('SELECT magento_product_id FROM {magento_products_queue} ORDER BY created ASC', 0, $limit);
  foreach ($result as $row) {
    $product_ids_to_import[] = $row->magento_product_id;
  }

  // If the queue is empty, we don't need to do anything else except log and
  // exit.
  if (empty($product_ids_to_import)) {
    $message = 'No products in the import queue';
    watchdog('magento_products', $message, array(), WATCHDOG_NOTICE);
    return;
  }

  $attributes = array();
  // define the attributes we are getting from magento. 
  // These will need to match attribute id's from within your magento install
  $attrs = array(
    132 => 'languages',
    133 => 'term',
    134 => 'timeslot',
    135 => 'exam_type',
    138 => 'course_type',
    139 => 'level',
    141 => 'parent_level',
    145 => 'lesson_format',
    162 => 'bundle_price_from'
    );

  $attributes = $magento->getAttributes($attrs);

  // get the attibute sets
  $attributes['ihb_set'] = $magento->getAttributeSets();


  // Load up our product batch from Magento and process.
  $products = $magento->getProducts($product_ids_to_import);

  $account = user_load(1);
  $products_updated_count = 0;
  $products_inserted_count = 0;

  foreach ($products as $product) {

  // $parent = $magento->getLinkedProduct($product['product_id']);
  //  var_dump($parent);
  //  $product['parent'] = $parent;

    magento_products_import_product($product, $products_inserted_count, $products_updated_count,$attributes);
  }
//  exit;

  // Even if we failed we want to delete all the products we queued up.
  // Otherwise, the queue may never shrink.
  db_query('DELETE FROM {magento_products_queue} WHERE magento_product_id IN (:magento_product_ids)', array(
    ':magento_product_ids' => $product_ids_to_import,
  ));

  $message = 'Magento Product import complete. Imported @added_count and updated @updated_count.';
  $variables = array(
    '@updated_count' => format_plural($products_updated_count, '@count product', '@count products'),
    '@added_count' => format_plural($products_inserted_count, '@count product', '@count products'),
  );
  watchdog('magento_products', $message, $variables, WATCHDOG_NOTICE);

  return $products_updated_count + $products_inserted_count;
}

/**
 * Import or update an individual Magento Product.
 *
 * @param  array $product
 *   A Magento product array, as returned by the API.
 * @param  integer $products_inserted_count
 *   If your calling method is counting successful updates and inserts, pass
 *   this variable in and it will be updated by reference.
 * @param  integer $products_updated_count
 *   If your calling method is counting successful updates and inserts, pass
 *   this variable in and it will be updated by reference.
 * @return mixed
 *   Node object on successful import or false otherwise.
 */
function magento_products_import_product($product, &$products_inserted_count = 0, &$products_updated_count = 0,$attributes) {
  try {
    $account = user_load(1);

    // Match to existing node if possible.
    $exists = db_query('SELECT entity_id FROM {magento_products} WHERE magento_product_id = :magento_product_id', array(
      ':magento_product_id' => $product['product_id'],
    ))->fetchField();
    if ($exists) {
      $node = node_load($exists);
    }
    else {
      // Save a new node.
      $node = new stdClass();
      $node->type = 'magento_product';
      node_object_prepare($node);
      $node->language = LANGUAGE_NONE;
      $node->uid = $account->uid;
      $node->name = $account->name;
      $node->comment = 0;
      $node = node_submit($node);
    }

    // In the case of node creation of update, make our changes.
    $node->status = $product['status'] = 1 && (int) $product['visibility'] >= MAGENTO_PRODUCTS_CATALOG_VISIBLITY;
    
    //  $node->status = $product['status'];

    $node->title = $product['name'];
    $node->body[LANGUAGE_NONE][0]['value']   = $product['description'];
    $node->body[LANGUAGE_NONE][0]['summary'] = text_summary($body);
    $node->body[LANGUAGE_NONE][0]['format']  = 'filtered_html';
    $url = variable_get('magento_store_url');
    $download_url = str_replace('/index.php','/media/catalog/product',$url);



    $node->field_magento_url[LANGUAGE_NONE][0]['value'] = $url . '/' . $product['url_path'];
    $node->field_magento_price[LANGUAGE_NONE][0]['value'] = $product['price'];
    $node->field_magento_bundle_price_from[LANGUAGE_NONE][0]['value'] = $product['bundle_price_from'];

    $node->field_magento_type[LANGUAGE_NONE][0]['value'] = $product['type'];
    $node->field_magento_set[LANGUAGE_NONE][0]['value'] = $attributes['ihb_set'][$product['set']];
    $node->field_magento_sku[LANGUAGE_NONE][0]['value'] = $product['sku'];

    if(!empty($product['ihb_mfl_term'])&& !empty($attributes['term'][$product['ihb_mfl_term']]))
    $node->field_magento_term[LANGUAGE_NONE][0]['value'] =   $attributes['term'][$product['ihb_mfl_term']];

    $node->field_magento_qty[LANGUAGE_NONE][0]['value'] = $product['qty'];


    $node->field_magento_visibility[LANGUAGE_NONE][0]['value'] = $product['visibility'];

    $node->field_magento_startdate[LANGUAGE_NONE][0]['value'] = $product['start_date'];
    $node->field_magento_enddate[LANGUAGE_NONE][0]['value'] = $product['end_date'];

    $node->field_magento_new_from_date[LANGUAGE_NONE][0]['value'] = $product['news_from_date'];
    $node->field_magento_new_to_date[LANGUAGE_NONE][0]['value'] = $product['news_to_date'];

    if(!empty($product['exam_speaking_dates']))
    $node->field_magento_dates_speaking[LANGUAGE_NONE][0]['value'] = $product['exam_speaking_dates'];

    if(!empty($product['exam_nonspeaking_dates']))    
    $node->field_magento_dates_nonspeaking[LANGUAGE_NONE][0]['value'] = $product['exam_nonspeaking_dates'];

    $node->field_magento_special_price[LANGUAGE_NONE][0]['value'] = $product['special_price'];
    $node->field_magento_special_from_date[LANGUAGE_NONE][0]['value'] = $product['special_from_date'];
    $node->field_magento_special_to_date[LANGUAGE_NONE][0]['value'] = $product['special_to_date'];

    if(!empty($product['closing_entry_date']))    
    $node->field_magento_closing_entry_date[LANGUAGE_NONE][0]['value'] = $product['closing_entry_date'];

    if(!empty($product['syllabus']))
    $node->field_magento_syllabus[LANGUAGE_NONE][0]['value'] = $download_url . $product['syllabus'];

    if(!empty($product['tutor']))
    $node->field_tutor[LANGUAGE_NONE][0]['value'] = $product['tutor'];


    if(isset($product['exam_type'])){
      // reset the array before refilling it.
      $node->field_certificate_type[LANGUAGE_NONE] = array();

      $exam_types = explode(',',$product['exam_type']);
        foreach($exam_types as $key => $value) {
          # code...
          $exam_typeId = _get_tid_from_term_name($attributes['exam_type'][$value],'certificate_type');

          if(!empty($exam_typeId))
          $node->field_certificate_type[LANGUAGE_NONE][0]['tid'] = $exam_typeId;
        }
      }

    if(isset($product['ihb_mfl_timeslot'])){
      // reset the array before refilling it.
      $node->field_magento_timeslot[LANGUAGE_NONE] = array();

      $timeslots = explode(',',$product['ihb_mfl_timeslot']);
        foreach($timeslots as $key => $value) {
          # code...
          $timeslotId = _get_tid_from_term_name($attributes['timeslot'][$value],'time_slots');

          if(!empty($timeslotId))
          $node->field_magento_timeslot[LANGUAGE_NONE][0]['tid'] = $timeslotId;
        }
      }

    if(isset($product['level'])){

      // reset the array before refilling it.
      $node->field_magento_level[LANGUAGE_NONE] = array();

      $levels = explode(',',$product['level']);

        foreach($levels as $key => $value) {
          # code...
          $levelId = _get_tid_from_term_name($attributes['level'][$value],'levels');

          if(!empty($levelId))
          $node->field_magento_level[LANGUAGE_NONE][0]['tid'] = $levelId;
        }
      }

      if(isset($product['languages'])){
        $languages = explode(',',$product['languages']);

        // reset the array before refilling it.
        $node->field_language[LANGUAGE_NONE] = array();

        foreach($languages as $key => $value) {
          # code...
          $languageId = _get_tid_from_term_name($attributes['languages'][$value],'language');

          if(!empty($languageId))
          $node->field_language[LANGUAGE_NONE][$key]['tid'] = $languageId;
        }
      }

      if(isset($product['course_type'])){
        $courseTypes = explode(',',$product['course_type']);

        // reset the array before refilling it.
        $node->field_course_type[LANGUAGE_NONE] = array();

        foreach($courseTypes as $key => $value) {
          # code...
          $courseTypeId = _get_tid_from_term_name($attributes['course_type'][$value],'course_type');

          if(!empty($courseTypeId))
          $node->field_course_type[LANGUAGE_NONE][$key]['tid'] = $courseTypeId;
        }
      }

    if(isset($product['parent_level'])){
      // reset the array before refilling it.
      $node->field_magento_parent_level[LANGUAGE_NONE] = array();

      $parent_levels = explode(',',$product['parent_level']);
        foreach($parent_levels as $key => $value) {
          # code...
          $parentLevelId = _get_tid_from_term_name($attributes['parent_level'][$value],'parent_level');

          if(!empty($parentLevelId))
          $node->field_magento_parent_level[LANGUAGE_NONE][0]['tid'] = $parentLevelId;
        }
      }


    if(isset($product['lesson_format'])){
      // reset the array before refilling it.
      $node->field_magento_lesson_format[LANGUAGE_NONE] = array();

      $formats = explode(',',$product['lesson_format']);
        foreach($formats as $key => $value) {
          # code...
          $formatId = _get_tid_from_term_name($attributes['lesson_format'][$value],'lesson_format');

          if(!empty($formatId))
          $node->field_magento_lesson_format[LANGUAGE_NONE][0]['tid'] = $formatId;
        }
      }

    // save category
    foreach($product['categories'] as $key => $value){
      $node->field_magento_category[LANGUAGE_NONE][$key]['value'] = $value;
    }

    // save parent
    //  $node->field_magento_parent[LANGUAGE_NONE][0]['value'] = $product['parent'];

    // Add in the first image URL to the product.
    if (isset($product['primary_image'])) {
      $node->field_magento_image_url[LANGUAGE_NONE][0]['value'] = $product['primary_image']['url'];
    }

    // Let other modules change what is imported.
    drupal_alter('magento_node', $node, $product);
    node_save($node);

    // Create or update the node.
    $bundle = variable_get('magento_product_node_content_type', 'magento_product');
    $values = array(
      ':entity_type' => 'node',
      ':entity_id' => $node->nid,
      ':bundle' => $bundle,
      ':magento_product_id' => $product['product_id'],
      ':synced' => time(),
    );

    // Create a link between Drupal or Magento or update the sync date on our
    // existing link. Also, logging.
    if ($exists) {
      db_query('UPDATE {magento_products} SET synced = :synced WHERE entity_id = :entity_id AND entity_type = :entity_type AND bundle = :bundle AND magento_product_id = :magento_product_id', $values);

      // Log to Drupal.
      $message = 'Updated existing Magento Product in Drupal for %title (nid @nid).';
      $variable = array('%title' => $node->title, '@nid' => $node->nid);
      watchdog('magento_products', $message, $variable, WATCHDOG_NOTICE, url('node/' . $node->nid));

      $products_updated_count++;
    }
    else {
      db_query('INSERT INTO {magento_products} (entity_id, entity_type, bundle, magento_product_id, synced) VALUES (:entity_id, :entity_type, :bundle, :magento_product_id, :synced)', $values);

      // Log to Drupal.
      $message = 'Created new Magento Product in Drupal for %title (nid @nid).';
      $variable = array('%title' => $node->title, '@nid' => $node->nid);
      watchdog('magento_products', $message, $variable, WATCHDOG_NOTICE, url('node/' . $node->nid));

      $products_inserted_count++;
    }

    return $node;
  }
  catch (Exception $e) {
    // Log to Drupal the full error message for debugging.
    $message = 'Unable to import Magento Product (@magento_product_id). Debugging: <pre>!debugging</pre>';
    $variables = array(
      '@magento_product_id' => $product['product_id'],
      '!debugging' => print_r($e, TRUE),
    );
    watchdog('magento_products', $message, $variables, WATCHDOG_ERROR);

    return FALSE;
  }
}
