<?php
/**
 * @file
 * online_learning.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function online_learning_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}
