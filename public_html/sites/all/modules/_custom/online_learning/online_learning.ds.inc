<?php
/**
 * @file
 * online_learning.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function online_learning_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'online_learning';
  $ds_field->label = 'Online Learning';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'ihbristol|block_online_learning',
    'block_render' => '2',
  );
  $export['online_learning'] = $ds_field;

  return $export;
}
