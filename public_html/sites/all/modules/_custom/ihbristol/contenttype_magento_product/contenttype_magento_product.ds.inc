<?php
/**
 * @file
 * contenttype_magento_product.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function contenttype_magento_product_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|magento_product|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'magento_product';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['node|magento_product|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function contenttype_magento_product_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|magento_product|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'magento_product';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'title',
        1 => 'field_magento_image_url',
        2 => 'body',
        3 => 'field_magento_price',
        4 => 'field_magento_url',
      ),
    ),
    'fields' => array(
      'title' => 'central',
      'field_magento_image_url' => 'central',
      'body' => 'central',
      'field_magento_price' => 'central',
      'field_magento_url' => 'central',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|magento_product|teaser'] = $ds_layout;

  return $export;
}
