<?php
/**
 * @file
 * contenttype_magento_product.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function contenttype_magento_product_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'First Certificate in English - March 2016',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => '0e9adfe1-6773-4d88-9b01-e991b41519c0',
  'ds_switch' => '',
  'type' => 'magento_product',
  'language' => 'und',
  'created' => 1454427279,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '38a8623e-f936-4c1c-aed6-881b186b10e6',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'First Certificate in EnglishExam',
        'summary' => 'First Certificate in EnglishExam',
        'format' => 'filtered_html',
        'safe_value' => '<p>First Certificate in EnglishExam</p>
',
        'safe_summary' => '<p>First Certificate in EnglishExam</p>
',
      ),
    ),
  ),
  'field_magento_category' => array(
    'und' => array(
      0 => array(
        'value' => 3,
      ),
    ),
  ),
  'field_magento_image_url' => array(),
  'field_magento_price' => array(
    'und' => array(
      0 => array(
        'value' => 145,
      ),
    ),
  ),
  'field_magento_qty' => array(
    'und' => array(
      0 => array(
        'value' => 50,
      ),
    ),
  ),
  'field_magento_sku' => array(
    'und' => array(
      0 => array(
        'value' => 'ihb_exam_cambridge_fce_2016march',
        'format' => NULL,
        'safe_value' => 'ihb_exam_cambridge_fce_2016march',
      ),
    ),
  ),
  'field_magento_startdate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-03-22 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_term' => array(),
  'field_magento_timeslot' => array(),
  'field_magento_type' => array(
    'und' => array(
      0 => array(
        'value' => 'simple',
        'format' => NULL,
        'safe_value' => 'simple',
      ),
    ),
  ),
  'field_magento_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://local.ihstore.co.uk/index.php/fce-march-2016.html',
        'format' => NULL,
        'safe_value' => 'http://local.ihstore.co.uk/index.php/fce-march-2016.html',
      ),
    ),
  ),
  'field_magento_visibility' => array(
    'und' => array(
      0 => array(
        'value' => 4,
      ),
    ),
  ),
  'field_parent' => array(),
  'field_magento_set' => array(
    'und' => array(
      0 => array(
        'value' => 'Cambridge Exams',
        'format' => NULL,
        'safe_value' => 'Cambridge Exams',
      ),
    ),
  ),
  'field_magento_enddate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-03-22 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_from_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-03-07 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_to_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-03-12 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_from_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-09 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_to_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-23 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_price' => array(
    'und' => array(
      0 => array(
        'value' => 100,
      ),
    ),
  ),
  'field_certificate_type' => array(
    'und' => array(
      0 => array(
        'tid' => 19,
        'uuid' => 'cde6f046-6664-472c-ac9b-08d5adfc1024',
      ),
    ),
  ),
  'field_language' => array(
    'und' => array(
      0 => array(
        'tid' => 1,
        'uuid' => 'd99aaeef-6172-48cb-b50e-ac5ed8e9c79a',
      ),
    ),
  ),
  'field_course_type' => array(
    'und' => array(
      0 => array(
        'tid' => 68,
        'uuid' => NULL,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => '',
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2016-02-02 15:34:39 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => '21/03/2016 to 19/04/2016 - Cambridge CELTA',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => 'c4ff68dd-8108-4fdc-990f-219a4708dbbb',
  'ds_switch' => '',
  'type' => 'magento_product',
  'language' => 'und',
  'created' => 1454592945,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '61b14de4-e2b2-4418-b057-70a62052cf52',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Cambridge CELTA
21/03/2016 to 19/04/2016 ',
        'summary' => 'Cambridge CELTA
21/03/2016 to 19/04/2016 ',
        'format' => 'filtered_html',
        'safe_value' => '<p>Cambridge CELTA<br />
21/03/2016 to 19/04/2016</p>
',
        'safe_summary' => '<p>Cambridge CELTA<br />
21/03/2016 to 19/04/2016</p>
',
      ),
    ),
  ),
  'field_magento_category' => array(),
  'field_magento_image_url' => array(),
  'field_magento_price' => array(
    'und' => array(
      0 => array(
        'value' => 1000,
      ),
    ),
  ),
  'field_magento_qty' => array(
    'und' => array(
      0 => array(
        'value' => 10,
      ),
    ),
  ),
  'field_magento_sku' => array(
    'und' => array(
      0 => array(
        'value' => 'ihb-course-cambridge-celta-2016-03',
        'format' => NULL,
        'safe_value' => 'ihb-course-cambridge-celta-2016-03',
      ),
    ),
  ),
  'field_magento_startdate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-03-21 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_term' => array(),
  'field_magento_timeslot' => array(),
  'field_magento_type' => array(
    'und' => array(
      0 => array(
        'value' => 'simple',
        'format' => NULL,
        'safe_value' => 'simple',
      ),
    ),
  ),
  'field_magento_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://local.ihstore.co.uk/index.php/21-03-2016-to-19-04-2016-cambridge-celta.html',
        'format' => NULL,
        'safe_value' => 'http://local.ihstore.co.uk/index.php/21-03-2016-to-19-04-2016-cambridge-celta.html',
      ),
    ),
  ),
  'field_magento_visibility' => array(
    'und' => array(
      0 => array(
        'value' => 4,
      ),
    ),
  ),
  'field_parent' => array(),
  'field_magento_set' => array(
    'und' => array(
      0 => array(
        'value' => 'Teacher Training',
        'format' => NULL,
        'safe_value' => 'Teacher Training',
      ),
    ),
  ),
  'field_magento_enddate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-04-19 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_from_date' => array(
    'und' => array(
      0 => array(
        'value' => NULL,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_to_date' => array(
    'und' => array(
      0 => array(
        'value' => NULL,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_from_date' => array(
    'und' => array(
      0 => array(
        'value' => NULL,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_to_date' => array(
    'und' => array(
      0 => array(
        'value' => NULL,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_price' => array(
    'und' => array(
      0 => array(
        'value' => NULL,
      ),
    ),
  ),
  'field_certificate_type' => array(
    'und' => array(
      0 => array(
        'tid' => 66,
        'uuid' => NULL,
      ),
    ),
  ),
  'field_language' => array(
    'und' => array(
      0 => array(
        'tid' => 1,
        'uuid' => 'd99aaeef-6172-48cb-b50e-ac5ed8e9c79a',
      ),
    ),
  ),
  'field_course_type' => array(
    'und' => array(
      0 => array(
        'tid' => 63,
        'uuid' => 'f2439fbe-bb63-49f1-9b14-fc2f6b975210',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => '',
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2016-02-04 13:35:45 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Preliminary English Test - February 2016',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => '3c39c1ee-27c8-4c52-8891-5e3b44c1a30d',
  'ds_switch' => '',
  'type' => 'magento_product',
  'language' => 'und',
  'created' => 1453898839,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'c44d11cc-f1d6-4f55-9708-93e86bc1f3f5',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Preliminary English Test Exam ',
        'summary' => 'Preliminary English Test Exam ',
        'format' => 'filtered_html',
        'safe_value' => '<p>Preliminary English Test Exam</p>
',
        'safe_summary' => '<p>Preliminary English Test Exam</p>
',
      ),
    ),
  ),
  'field_magento_category' => array(
    'und' => array(
      0 => array(
        'value' => 3,
      ),
      1 => array(
        'value' => 3,
      ),
    ),
  ),
  'field_magento_image_url' => array(),
  'field_magento_price' => array(
    'und' => array(
      0 => array(
        'value' => 85,
      ),
    ),
  ),
  'field_magento_qty' => array(
    'und' => array(
      0 => array(
        'value' => 99,
      ),
    ),
  ),
  'field_magento_sku' => array(
    'und' => array(
      0 => array(
        'value' => 'fhb_exam_cambridge_pet_2016feb',
        'format' => NULL,
        'safe_value' => 'fhb_exam_cambridge_pet_2016feb',
      ),
    ),
  ),
  'field_magento_startdate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-22 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_term' => array(),
  'field_magento_timeslot' => array(),
  'field_magento_type' => array(
    'und' => array(
      0 => array(
        'value' => 'simple',
        'format' => NULL,
        'safe_value' => 'simple',
      ),
    ),
  ),
  'field_magento_url' => array(
    'und' => array(
      0 => array(
        'value' => 'http://local.ihstore.co.uk/index.php/pet-february-2016.html',
        'format' => NULL,
        'safe_value' => 'http://local.ihstore.co.uk/index.php/pet-february-2016.html',
      ),
    ),
  ),
  'field_magento_visibility' => array(
    'und' => array(
      0 => array(
        'value' => 4,
      ),
    ),
  ),
  'field_parent' => array(),
  'field_magento_set' => array(
    'und' => array(
      0 => array(
        'value' => 'Cambridge Exams',
        'format' => NULL,
        'safe_value' => 'Cambridge Exams',
      ),
    ),
  ),
  'field_magento_enddate' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-22 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_from_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-07 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_new_to_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-12 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_from_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-09 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_to_date' => array(
    'und' => array(
      0 => array(
        'value' => '2016-02-23 00:00:00',
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_magento_special_price' => array(
    'und' => array(
      0 => array(
        'value' => 60,
      ),
    ),
  ),
  'field_certificate_type' => array(
    'und' => array(
      0 => array(
        'tid' => 19,
        'uuid' => 'cde6f046-6664-472c-ac9b-08d5adfc1024',
      ),
    ),
  ),
  'field_language' => array(
    'und' => array(
      0 => array(
        'tid' => 1,
        'uuid' => 'd99aaeef-6172-48cb-b50e-ac5ed8e9c79a',
      ),
    ),
  ),
  'field_course_type' => array(
    'und' => array(
      0 => array(
        'tid' => 68,
        'uuid' => NULL,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => '',
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2016-01-27 12:47:19 +0000',
);
  return $nodes;
}
