<?php
/**
 * @file
 * contenttype_magento_product.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function contenttype_magento_product_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'One-to-one English',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '014b821d-c4a3-4780-bc25-988bf7c9836a',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Intermediate 1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 11,
    'uuid' => '06f1204a-0865-4de0-8dd5-c4c92beeaa63',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beginner 1',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '07adbf9e-3b62-4da0-91ae-931ccdcf9739',
    'vocabulary_machine_name' => 'levels',
  );
  $terms[] = array(
    'name' => 'Pre-intermediate',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 8,
    'uuid' => '0e04b18f-86a4-4735-81be-7af8fa59f0e9',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Intermediate 3',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 13,
    'uuid' => '189e7d2a-cfef-463f-999d-3d120f7712cc',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Elementary 2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '267baf55-53cc-4021-9d5c-512ebe8f43e9',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Monday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2691ff9e-4483-4a60-a0b5-34eb63cde8f3',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beginner',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '299d788f-e52e-48ea-9f5d-cb72a593c4fb',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Elementary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '350429b4-2ce1-451b-81bd-780f19f6b07e',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'German',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3b001d26-5a46-4837-92e5-ac69fa276cb8',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Saturday 10:45 - 11:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '412553d5-a2c1-4fca-9d93-d5318be74495',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Advanced',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 14,
    'uuid' => '47bd035d-ca35-4e34-84c3-6bd0a1f024e8',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '4c7e86a0-bbd1-44fb-8819-b393825bd42a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '507dc1df-690f-4296-a0fe-6e010ff34fdf',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Elementary 1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '52ac4558-87f4-4c56-a762-be1cc9995514',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'French',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5a35b89e-41fa-4677-853f-57abbb7155ba',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '5b7d17f2-122d-4f31-ac9a-8ec145efdfe6',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tuesday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '5c82fd07-3ec6-4511-9b17-fe0cbbe6152f',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Junior English course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '64bdc8ee-2f5c-4a13-9e97-4f721384b28b',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Mandarin',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '68f6a3aa-a521-4a13-8b4c-f351c16d98b4',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Japanese',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '6a3fdc19-0dca-4cbc-9af3-a1fe52a113fb',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'P.E.T',
    'description' => 'Preliminary English Test',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '6cb20ce3-5e30-4a7c-8c01-9e78fd56bf5e',
    'vocabulary_machine_name' => 'certificate_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Thursday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '75b5ec6d-7eaf-4a9f-82da-e54f0fd3df7c',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Italian',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '76b63bb0-7cc9-497d-883f-1bc0e7c572c1',
    'vocabulary_machine_name' => 'language',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Conversation 2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 16,
    'uuid' => '77468944-e9d9-45df-8c8f-3446a8458629',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Foreign Teachers of English course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '77787653-900f-4b3d-8230-7d5542b0a406',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Intermediate 2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 12,
    'uuid' => '7bbaeb07-58e0-4d80-b81f-50c9c7246d5f',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Wednesday 18:15 - 19:45',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '8a602335-55b4-47b1-8891-afa6b47b5b3a',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Full-time IELTS Exam course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '8ded038b-b044-4ee1-8823-5fa65b7f8a3a',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'General English',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '8fc0c335-0c34-46ba-9610-7b79d70b5c3f',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'C.P.E',
    'description' => 'Certificate of Proficiency in English',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '9aa21661-36fc-4785-ad1a-f3bf0090fc36',
    'vocabulary_machine_name' => 'certificate_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Intermediate',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 9,
    'uuid' => 'a161f8a1-02a4-4446-ba5a-3d5b88d27593',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Cambridge Exam summer course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'a6709344-a8f4-4f06-b477-aadb841f975a',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'C.A.E',
    'description' => 'Certificate in Advanced English',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b0e0052d-64aa-4d83-a2dd-6e535db364f7',
    'vocabulary_machine_name' => 'certificate_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beginner 1 - Foundation',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'b2455e2d-8ca6-4ff9-b66a-636ee3c9cfa2',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'B.U.L.A.T.S',
    'description' => 'Business language testing service',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b9aab574-d685-4fe3-9d1f-f6b940bd3d06',
    'vocabulary_machine_name' => 'certificate_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Full-time Intensive English course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'c9471393-d1d5-4098-8ca9-1d24fe0b6976',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beginner 2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'cd909e2c-194a-4691-b74d-ce2058ab0fb9',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'F.C.E',
    'description' => 'First Certificate in English',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'cde6f046-6664-472c-ac9b-08d5adfc1024',
    'vocabulary_machine_name' => 'certificate_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beginner 3',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'd44c205c-9088-4135-b72c-89edf0245ab3',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => '',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd5957578-b01f-4008-9f33-970863075cfb',
    'vocabulary_machine_name' => 'language',
  );
  $terms[] = array(
    'name' => 'Conversation 1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 15,
    'uuid' => 'd75db0c2-2372-4ecb-9efc-ec32087c9dbf',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'English',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'd99aaeef-6172-48cb-b50e-ac5ed8e9c79a',
    'vocabulary_machine_name' => 'language',
  );
  $terms[] = array(
    'name' => 'Full-time Cambridge Exam course',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'ded8a811-f954-4e76-8664-120ede20906e',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Upper-intermediate',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 10,
    'uuid' => 'e0b2ef7e-5c58-4894-8bf5-0547ea576b45',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Monday 20:00 - 21:30',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'e82e1a54-5612-45a9-8627-1d2aa12d1f3b',
    'vocabulary_machine_name' => 'time_slots',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Elementary 3',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => 'ec9d035c-5eed-4fbf-8a79-1479b936fd8c',
    'vocabulary_machine_name' => 'levels',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Cambridge CELTA',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f2439fbe-bb63-49f1-9b14-fc2f6b975210',
    'vocabulary_machine_name' => 'course_type',
    'field_description' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
