<?php
/**
 * @file
 * contenttype_magento_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function contenttype_magento_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_features_date_type_default().
 */
function contenttype_magento_product_features_date_type_default() {
  $fdt_config = array();

  $fdt_config[] = array(
    'is_new' => FALSE,
    'module' => '',
    'type' => 'medium_date_only',
    'title' => 'Medium Date Only',
    'locked' => 0,
  );
  return $fdt_config;
}

/**
 * Implements hook_node_info().
 */
function contenttype_magento_product_node_info() {
  $items = array(
    'magento_product' => array(
      'name' => t('Magento Product'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
