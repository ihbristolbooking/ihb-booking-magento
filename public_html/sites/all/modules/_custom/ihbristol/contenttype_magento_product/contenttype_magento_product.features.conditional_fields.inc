<?php
/**
 * @file
 * contenttype_magento_product.features.conditional_fields.inc
 */

/**
 * Implements hook_conditional_fields_default_fields().
 */
function contenttype_magento_product_conditional_fields_default_fields() {
  $items = array();

  $items["node:magento_product:0"] = array(
    'entity' => 'node',
    'bundle' => 'magento_product',
    'dependent' => 'field_magento_special_price',
    'dependee' => 'field_magento_special_from_date',
    'options' => array(
      'state' => 'visible',
      'condition' => 'value',
      'grouping' => 'AND',
      'values_set' => 1,
      'value' => array(),
      'values' => array(),
      'value_form' => array(),
      'effect' => 'show',
      'effect_options' => array(),
      'element_view' => array(
        1 => 1,
        2 => 2,
        5 => 0,
        3 => 0,
        4 => 0,
      ),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit' => array(
        1 => 1,
        2 => 0,
        3 => 0,
      ),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'selector' => '',
    ),
  );

  return $items;
}
