<?php
/**
 * @file
 * phrase_of_the_day.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phrase_of_the_day_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function phrase_of_the_day_node_info() {
  $items = array(
    'phrase_of_the_day' => array(
      'name' => t('Phrase Of The Day'),
      'base' => 'node_content',
      'description' => t('Content type for the English \'Phrase Of The Day\' '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
