<?php
/**
 * @file
 * phrase_of_the_day.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function phrase_of_the_day_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ex1|node|phrase_of_the_day|default';
  $field_group->group_name = 'group_ex1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Example 1',
    'weight' => '5',
    'children' => array(
      0 => 'field_line_ex1_1',
      1 => 'field_line_ex1_2',
      2 => 'field_line_ex1_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ex1 field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ex1|node|phrase_of_the_day|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ex1|node|phrase_of_the_day|form';
  $field_group->group_name = 'group_ex1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Example 1',
    'weight' => '6',
    'children' => array(
      0 => 'field_line_ex1_1',
      1 => 'field_line_ex1_2',
      2 => 'field_line_ex1_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ex1 field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ex1|node|phrase_of_the_day|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ex2|node|phrase_of_the_day|default';
  $field_group->group_name = 'group_ex2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Example 2',
    'weight' => '6',
    'children' => array(
      0 => 'field_line_ex2_1',
      1 => 'field_line_ex2_2',
      2 => 'field_line_ex2_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ex2 field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ex2|node|phrase_of_the_day|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ex2|node|phrase_of_the_day|form';
  $field_group->group_name = 'group_ex2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Example 2',
    'weight' => '7',
    'children' => array(
      0 => 'field_line_ex2_1',
      1 => 'field_line_ex2_2',
      2 => 'field_line_ex2_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ex2 field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ex2|node|phrase_of_the_day|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_usage|node|phrase_of_the_day|default';
  $field_group->group_name = 'group_usage';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Usage',
    'weight' => '3',
    'children' => array(
      0 => 'field_written',
      1 => 'field_spoken',
      2 => 'field_formal',
      3 => 'field_informal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-usage field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_usage|node|phrase_of_the_day|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_usage|node|phrase_of_the_day|form';
  $field_group->group_name = 'group_usage';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phrase_of_the_day';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Usage',
    'weight' => '5',
    'children' => array(
      0 => 'field_written',
      1 => 'field_spoken',
      2 => 'field_formal',
      3 => 'field_informal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-usage field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_usage|node|phrase_of_the_day|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Example 1');
  t('Example 2');
  t('Usage');

  return $field_groups;
}
