<?php
/**
 * @file
 * phrase_of_the_day.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function phrase_of_the_day_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'phrase_of_the_day-example11-explode';
  $feeds_tamper->importer = 'phrase_of_the_day';
  $feeds_tamper->source = 'Example11';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '*',
    'limit' => '',
    'real_separator' => '*',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['phrase_of_the_day-example11-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'phrase_of_the_day-example21-explode';
  $feeds_tamper->importer = 'phrase_of_the_day';
  $feeds_tamper->source = 'Example21';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '*',
    'limit' => '',
    'real_separator' => '*',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['phrase_of_the_day-example21-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'phrase_of_the_day-image-find_replace';
  $feeds_tamper->importer = 'phrase_of_the_day';
  $feeds_tamper->source = 'Image';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '{filedir_4}',
    'replace' => 'sites/default/files/import_images/filedir_4/',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['phrase_of_the_day-image-find_replace'] = $feeds_tamper;

  return $export;
}
