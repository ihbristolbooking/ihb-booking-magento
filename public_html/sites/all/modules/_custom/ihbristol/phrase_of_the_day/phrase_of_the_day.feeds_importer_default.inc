<?php
/**
 * @file
 * phrase_of_the_day.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function phrase_of_the_day_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'phrase_of_the_day';
  $feeds_importer->config = array(
    'name' => 'Phrase of the day',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => '|',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'url_title',
            'target' => 'url',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Date',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Month',
            'target' => 'field_month',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Day',
            'target' => 'field_day',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Explanation',
            'target' => 'field_phrase_explanation',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Youtube',
            'target' => 'field_youtube_embed',
            'format' => 'full_html',
          ),
          8 => array(
            'source' => 'Image',
            'target' => 'field_background:uri',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Written',
            'target' => 'field_written',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Spoken',
            'target' => 'field_spoken',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Formal',
            'target' => 'field_formal',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Informal',
            'target' => 'field_informal',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Example11',
            'target' => 'field_line_ex1_1',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Example21',
            'target' => 'field_line_ex2_1',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'phrase_of_the_day',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['phrase_of_the_day'] = $feeds_importer;

  return $export;
}
