<?php
/**
 * @file
 * courses.features.inc
 */

/**
 * Implements hook_node_info().
 */
function courses_node_info() {
  $items = array(
    'courses' => array(
      'name' => t('Courses'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
