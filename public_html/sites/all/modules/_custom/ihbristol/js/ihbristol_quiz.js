( function($) {     

	console.log('quiz'); 



	function indexToChar(i) {
	 return String.fromCharCode(i+97); //97 in ASCII is 'a', so i=0 returns 'a', 			                                    // i=1 returns 'b', etc
	}
	                                    
	var count = 1;






	$(document).ready(function(){

		var i = 0;

		$('.page-node-8564 .quiz-question-quiz-page .quiz-page-image div.row div').each(function(){

			$(this).attr('data-title',indexToChar(i));
			i++;

		})

		$.preloadImages = function() {
		  for (var i = 0; i < arguments.length; i++) {
		    $("<img />").attr("src", arguments[i]);
		  }
		}

//		$.preloadImages("/sites/all/themes/ihbristol/img/spinner.gif");

//		$('.role-quiz-participant .view-id-online_test .view-mode-teaser .quiz-start-link').each(function(){

		//	$(this).html('Starting Test '+'<img src="/sites/all/themes/ihbristol/img/spinner.gif" />');	

		//	var url = $(this).attr('href');

		//	setTimeout(function(){ 
		//		window.location = url;
		//	 }, 2000);

//		})


		$('div.entity-quiz-result-answer div.correct').parent('td').css('background-color','#dff0d8').parent('tr').css('background-color','#dff0d8');

		var title = $('div.field-name-title h2').text();

	//	if(title.length <2)
	//	$('h2').remove();

		if (title.indexOf('Quiz Page') == 0)
			$('div.field-name-title h2').remove();

		$('.page-node-quiz-results fieldset.form-wrapper').each(function(){

			var type = $(this).find('div.question_type_name').text();

			if(type=='Quiz page'){
				$(this).find('table').remove();
				$(this).find('.form-type-item').remove();
				$(this).find('legend').remove();

				var title = $(this).find('h2').text();

				if(title=='')
				$(this).find('h2').remove();					
			}
		})
	})


	$('#quiz-question-answering-form').ready(function(){   

		$('#edit-navigation').before('<fieldset id="questions"></fieldset><fieldset id="answers"><p>Please enter your answers in CAPITALS. Lower case answers will be marked as incorrect.</p><h3>Answers</h3><ol></ol></fieldset>');

		$('.quiz-question-short-answer .sentence,.quiz-question-word-formation .sentence').each(function(){
			$(this).find('span.counter').attr('data-counter',count);
			$(this).appendTo('#quiz-question-answering-form fieldset#questions');	
			count++;
		});


		// hide answers block if its a multiple choice
		$('.quiz-question-multichoice').each(function(){
			$('#answers').css('display','none');
		})



		$('.quiz-question-keyword-transformation').each(function(){

			$('#questions').addClass('full');

			$(this).attr('data-counter',count);
			$(this).appendTo('#quiz-question-answering-form fieldset#questions');	

			$(this).find('span.wr').attr('data-counter',count);

			var sentence = $(this).find('span.sentence');
			var	operative_word = $(this).find('.operative_word');

			$(sentence).insertAfter(operative_word);

			count++;
		});


		$('.quiz-question-gapped-sentence').each(function(){


			$('#questions').addClass('full');

			$(this).appendTo('#quiz-question-answering-form fieldset#questions');	

			var sentence = $(this).find('span.sentence').children('p').attr('data-counter',count);
			$(this).children('p:last-child').attr('data-counter',count);
			$(this).find('span.wr').children('span.counter').attr('data-counter',count);

			count++;

		})

		$('.quiz-question-multichoice').each(function(){

			var questionId = $(this).find('span.sentence').attr('data-question-id');

			$(this).attr('data-counter',count);

			if(questionId !=0)
			$(this).attr('data-counter',questionId);				

		


			count++;

		})

		// add key for multichoice question

		var firstCount = $('span.sentence').first().attr('data-question-id');

		var max = parseInt(firstCount)+parseInt(count-2);

		if($('.field_question_0').length >0){
			$('.quiz-question-multichoice').parents('form').children('div').before('<div class="key">For questions '+firstCount+'-'+max+', read the text below and decide which answer (A, B, C or D) best fits each gap. There is an example at the beginning (0).</div>').children('div:first-child').after('<h3>Answers</h3>');
		} else {
			$('.quiz-question-multichoice').parents('form').children('div').before('<div class="key">For questions '+firstCount+'-'+max+', read the text below and decide which answer (A, B, C or D) best fits each gap. <!-- There is an example at the beginning (0).--></div>').children('div:first-child').after('<h3>Answers</h3>');
		}

		$('.quiz-question-word-formation').parents('form').children('div').before('<div class="key">For questions 1-'+(count-1)+', read the text below. Use the word given in capitals at the end of some of the lines to form a word that fits in the gap in the same line. There is an example at the beginning (0).</div>');


		$('.quiz-question-keyword-transformation').parents('form').children('div').before('<div class="key">For questions 1-'+(count-1)+', complete the second sentence so that it has a similar meaning to the first sentence, using the word given. Do not change the word given. You must use between three and eight words, including the word given. Here is an example (0).</div>');

		$('.quiz-question-gapped-sentence').parents('form').children('div').before('<div class="key">For questions 1-'+(count-1)+', think of one word only which can be used appropriately in all three sentences. Here is an example (0).</div>');

		$('.quiz-question-gapped-sentence .form-type-textfield, .quiz-question-keyword-transformation .form-type-textfield, .quiz-question-short-answer .form-type-textfield,.quiz-question-word-formation .form-type-textfield').each(function(){
			$(this).appendTo('#quiz-question-answering-form fieldset#answers ol').wrap('<li />');		
		})

		$('.quiz-page-image img').each(function(){

			var title = $(this).attr('title');
			$(this).parent('div').attr('data-title',title);
		})

		$('.quiz-question-multichoice').each(function(){

			var i = 0;

			$(this).find('.table-responsive .multichoice-row').each(function(){

				$(this).attr('data-label',indexToChar(i));

				i++;

			})
		})
	})
	
})( jQuery );