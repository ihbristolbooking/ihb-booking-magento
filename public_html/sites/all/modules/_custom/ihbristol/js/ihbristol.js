( function($) {   


	$(document).ready(function(){



		// js to deal with accordians in page content
jQuery.fn.sectionHeadings = function() {
    // Usually iterate over the items and return for chainability
    // 'this' is the elements returns by the selector

    var sectionHeadings = '';
    var i = 0;
    var obj = this;
    var parentId = obj.attr('id');

    obj.attr('class','section_headings');


    function isInView(elem){

    	var elemOffset = $(elem).offset().top;
    	var scrollTop = $(window).scrollTop();
    	var winPos = $(window).scrollTop() + $(window).height();
    	var elemHeight = $(elem).height();

//    	console.log(elemOffset);
//    	console.log(scrollTop);
//    	console.log(winPos);
//    	console.log(elemHeight);

   		return elemOffset < winPos;
	}


    /* set up the initial headings */
    obj.children('section').each(function() { 
         // do something to each item matching the selector
         var string = $(this).children('h3').text();

         sectionHeadings +='<li><span data-class="section'+i+'">'+string+'</span></li>';

         $(this).hide();
         $(this).addClass('section'+i)

         i++;

    })

    obj.prepend('<nav><ul>'+sectionHeadings+'</ul></nav>');

    /* show the first item */
	obj.children('.section0').show();   

	obj.children('nav').children('ul').children('li').children('span').on('click',function(){

		var showClass = $(this).data('class');


		$('#'+parentId).children('nav').children('ul').children('li').removeClass('clicked');
		$(this).parent('li').addClass('clicked');

		$('#'+parentId+' section:visible').hide();
		$('#'+parentId+' .'+showClass).fadeIn('slow', function(){

			// check we've finished
		var res = isInView($(this).children('h3'));

		if(res==false){
			// scroll up
			$('html, body').animate({scrollTop: $(obj).offset().top}, 1000);
			}
		});
	})

    return;
}




	$('.sections').each(function(){

		 var container_id = 'r'+ (new Date).getTime();

		$(this).attr('id',container_id);

		$('#'+container_id).sectionHeadings();

	})




$('#field-password-add-more-wrapper .help-block,#field-confirm-password-add-more-wrapper .help-block').hide();


		$('.view-display-id-block_apply_for_an_exam .views-table').each(function(){

			var back_to_top = '<p>If you enter for the exam after the closing entry date, there will be a charge of £45.</p><p><a class="top" href="#top">Back to top</a></p>';


			$(this).after(back_to_top);


		})



		/*$('.view-mode-homepage_slider .row').on('click','*',function(){

			var findStr ="/content/homepage-slider";
			var url = $(this).parents('.view-mode-homepage_slider').attr('about');

			if(url.includes(findStr)){

				console.log('local node - dont display')

			} else {
				window.location = url;
			}
		})*/

	})

})( jQuery );
