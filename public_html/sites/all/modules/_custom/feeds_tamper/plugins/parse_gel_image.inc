<?php

/**
 * @file
 * Parses json HTML Description and returns a image string.
 */


$plugin = [
  'form' => 'feeds_tamper_parse_gel_image_form',
  'callback' => 'feeds_tamper_parse_gel_image_callback',
  'name' => 'Parse Image from GEL description field',
  'multi' => 'direct',
  'category' => 'IHBristol',
];

/**
 * @param $importer
 * @param $element_key
 * @param $settings
 *
 * @return array
 */
function feeds_tamper_parse_gel_image_form($importer, $element_key, $settings) {
  return [
    'help' => [
      '#markup' => t('Parses Json in HTML Description and returns an image string.'),
    ],
  ];
}

/**
 * @param $result
 * @param $item_key
 * @param $element_key
 * @param $field
 * @param $settings
 * @param $source
 */
function feeds_tamper_parse_gel_image_callback(
  $result,
  $item_key,
  $element_key,
  &$field,
  $settings,
  $source
) {
  $field = feeds_tamper_parse_gel_image_extract($field);
}


/**
 * @param $description
 *
 * @return string
 */
function feeds_tamper_parse_gel_image_extract($description) {

  $result = feeds_tamper_parse_extract_field($description, 'field_picture');

  return 'http://learn2.guidedelearning.net/' . $result['filepath'];

}