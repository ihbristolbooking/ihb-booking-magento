<?php


/**
 * @file
 * Parses HTML Description and returns a date string.
 */

$plugin = [
  'form' => 'feeds_tamper_parse_gel_date_form',
  'callback' => 'feeds_tamper_parse_gel_date_callback',
  'name' => 'Parse Date from GEL description field',
  'multi' => 'direct',
  'category' => 'IHBristol',
];

/**
 * @param $importer
 * @param $element_key
 * @param $settings
 *
 * @return array
 */
function feeds_tamper_parse_gel_date_form($importer, $element_key, $settings) {
  return [
    'help' => [
      '#markup' => t('Parses HTML Description and returns a date string.'),
    ],
    'fieldType' => [
      '#type' => 'textfield',
      '#title' => t('Set Field Type parse in object'),
      '#default_value' => isset($settings['fieldType']) ? $settings['fieldType'] : FALSE,
    ],
  ];
}

/**
 * @param $result
 * @param $item_key
 * @param $element_key
 * @param $field
 * @param $settings
 * @param $source
 */
function feeds_tamper_parse_gel_date_callback(
  $result,
  $item_key,
  $element_key,
  &$field,
  $settings,
  $source
) {

  if ($settings['fieldType']) {
    $field = feeds_tamper_parse_gel_date_extract($field,$settings['fieldType']);
  }
}

/**
 * @param $description
 * @param $type
 *
 * @return string
 */
function feeds_tamper_parse_gel_date_extract($description, $type) {

  if ($type == 'start') {
    $dateField = 'field_date';
    $timeField = 'field_time';
  };

  if ($type == 'end') {
    $dateField = 'field_end_date';
    $timeField = 'field_end_time';
  }

  $date = feeds_tamper_parse_extract_field($description, $dateField);
  $time = feeds_tamper_parse_extract_field($description, $timeField);

  $date = str_replace('T00:00:00', '', $date);
  $time = str_replace('1970-00-00T', '', $time);

  if (is_array($date)) {
    $date = feeds_tamper_parse_extract_field($description,
      'field_date');
    $date = str_replace('T00:00:00', '', $date);
  }

  return $date . ' ' . $time;

}