<?php

/**
 * @file
 * Parses HTML Description and returns a location string.
 */

$plugin = [
  'form' => 'feeds_tamper_parse_gel_description_form',
  'callback' => 'feeds_tamper_parse_gel_description_callback',
  'name' => 'Parse and clean specific field from GEL description field',
  'multi' => 'direct',
  'category' => 'IHBristol',
];

/**
 * @param $importer
 * @param $element_key
 * @param $settings
 *
 * @return array
 */
function feeds_tamper_parse_gel_description_form($importer, $element_key, $settings) {
  return [
    'help' => [
      '#markup' => t('Parses HTML Description and returns field from JSON object.'),
    ],
    'fieldName' => [
      '#type' => 'textfield',
      '#title' => t('Set Field to parse in object'),
      '#default_value' => isset($settings['fieldName']) ? $settings['fieldName'] : FALSE,
    ],
  ];
}

/**
 * @param $result
 * @param $item_key
 * @param $element_key
 * @param $field
 * @param $settings
 * @param $source
 */
function feeds_tamper_parse_gel_description_callback(
  $result,
  $item_key,
  $element_key,
  &$field,
  $settings,
  $source
) {
  if ($settings['fieldName']) {
    $field = feeds_tamper_parse_gel_description_extract($field,$settings['fieldName']);
  }
}





/**
 * @param $description
 * @param $fieldName
 *
 * @return string
 */
function feeds_tamper_parse_gel_description_extract($description, $fieldName) {

  $result = feeds_tamper_parse_extract_field($description, $fieldName);

  $result = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $result);

  $result = str_replace(['<p>&nbsp;</p>', "\n", "\r"], '', $result);
  $result = strip_tags($result, '<p> <br>');
  $result = str_replace('\u00a3', '£', $result);

  return $result;
}



