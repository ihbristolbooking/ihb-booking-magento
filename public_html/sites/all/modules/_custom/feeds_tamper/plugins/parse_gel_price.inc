<?php

/**
 * @file
 * Parses HTML Description and returns a string containing a formatted price.
 */

$plugin = [
  'form' => 'feeds_tamper_parse_gel_price_form',
  'callback' => 'feeds_tamper_parse_gel_price_callback',
  'name' => 'Parse Price from GEL description field',
  'multi' => 'direct',
  'category' => 'IHBristol',
];

/**
 * @param $importer
 * @param $element_key
 * @param $settings
 *
 * @return array
 */
function feeds_tamper_parse_gel_price_form($importer, $element_key, $settings) {
  return [
    'help' => [
      '#markup' => t('Parses Json in HTML Description and returns a string containing a formatted price.'),
    ],
  ];
}

/**
 * @param $result
 * @param $item_key
 * @param $element_key
 * @param $field
 * @param $settings
 * @param $source
 */
function feeds_tamper_parse_gel_price_callback(
  $result,
  $item_key,
  $element_key,
  &$field,
  $settings,
  $source
) {
  $field = feeds_tamper_parse_gel_cost_extract($field);
}


/**
 * @param $description
 *
 * @return mixed
 */
function feeds_tamper_parse_gel_cost_extract($description) {

  $result = feeds_tamper_parse_extract_field($description, 'field_paid');

  $result = str_replace('\u00a3', '£', $result);

  return $result;

}
