<?php

/**
 * @file
 * Parses HTML Description and returns a taxonomy term
 */

$plugin = [
  'form' => 'feeds_tamper_parse_gel_age_group_form',
  'callback' => 'feeds_tamper_parse_gel_age_group_callback',
  'name' => 'Parse Age Group from GEL description field',
  'multi' => 'direct',
  'category' => 'IHBristol',
];

/**
 * @param $importer
 * @param $element_key
 * @param $settings
 *
 * @return array
 */
function feeds_tamper_parse_gel_age_group_form(
  $importer,
  $element_key,
  $settings
) {
  return [
    'help' => [
      '#markup' => t('Parses Json in HTML Description and returns a taxonomy term.'),
    ],
  ];
}

/**
 * @param $result
 * @param $item_key
 * @param $element_key
 * @param $field
 * @param $settings
 * @param $source
 */
function feeds_tamper_parse_gel_age_group_callback(
  $result,
  $item_key,
  $element_key,
  &$field,
  $settings,
  $source
) {
  $field = feeds_tamper_parse_gel_age_limit_extract($field);
}


/**
 * @param $description
 *
 * @return mixed
 */
function feeds_tamper_parse_gel_age_limit_extract($description) {

  $result = feeds_tamper_parse_extract_field($description, 'field_age_limit');

  return feeds_tamper_map_their_values_to_ours($result);

}

/**
 * @param $input
 *
 * @return mixed
 */
function feeds_tamper_map_their_values_to_ours($input) {

  $mapper = [
    "No age-limit" => ["Junior","Adult"],
    "Under 18" => "Junior",
    "Over 18" => "Adult",
  ];

  return isset($mapper[$input]) ? $mapper[$input] : $input;
}
