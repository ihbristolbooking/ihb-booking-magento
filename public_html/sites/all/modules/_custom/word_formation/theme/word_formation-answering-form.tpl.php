<?php
/**
 * @file
 * Handles the layout of the word_formation answering form.
 *
 *
 * Variables available:
 * - $form
 */
print drupal_render($form);

?>