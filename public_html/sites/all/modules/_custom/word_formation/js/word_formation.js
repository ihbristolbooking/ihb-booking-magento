( function($) {      
	                                    
	var count = 1;

	$('#quiz-question-answering-form').ready(function(){   

		$('#edit-navigation').before('<fieldset id="questions"></fieldset><fieldset id="answers"><p>Please enter your answers in CAPITALS. Lower case answers will be marked as incorrect.</p><h3>Answers</h3><ol></ol></fieldset>');

		$('.quiz-question-word-formation .sentence').each(function(){
			$(this).children('span.wr').children('span.counter').attr('data-counter',count);
			$(this).appendTo('#quiz-question-answering-form fieldset#questions');	
			count++;
		});

		$('.form-type-textfield').each(function(){
			$(this).appendTo('#quiz-question-answering-form fieldset#answers ol').wrap('<li />');		
		})
	})
	
})( jQuery );