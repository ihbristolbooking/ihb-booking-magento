<?php
/**
 * @file
 * teacher_training_application.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function teacher_training_application_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'entityform|teacher_training_application|form';
  $ds_layout->entity_type = 'entityform';
  $ds_layout->bundle = 'teacher_training_application';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'field_tt_course_date',
        1 => 'group_your_details',
        2 => 'field_address',
        3 => 'group_emergency_contact',
        4 => 'group_further_info',
        5 => 'group_celta',
        6 => 'field_extra_info',
        7 => 'field_dyslexia',
        8 => 'field_special_conditions',
        9 => 'field_html_insurance',
        10 => 'field_needs_and_requirements',
        11 => 'field_lead_generation',
        12 => 'field_marketing_other',
        13 => 'field_offences',
        14 => 'field_special_more_info',
        15 => 'field_occupation',
        16 => 'field_terms',
        17 => 'group_work_experience',
        18 => 'field_alternative_course_date',
        19 => 'group_education',
        20 => 'field_course_how',
        21 => 'group_teaching_experience',
        22 => 'field_accommodation_help',
        23 => 'group_languages',
        24 => 'field_emergency_name',
        25 => 'field_emergency_telephone',
        26 => 'field_secondary_education',
        27 => 'field_higher_education',
        28 => 'field_professional_training',
        29 => 'field_experience_efl',
        30 => 'field_experience_other_subject',
        31 => 'field_experience_teacher_trainin',
        32 => 'field_applicant',
        33 => 'field_sex',
        34 => 'field_date_of_birth',
        35 => 'field_nationality',
        36 => 'field_telephone',
        37 => 'field_mobile_telephone',
        38 => 'field_email',
        39 => 'field_languages_known',
        40 => 'field_work_experience',
      ),
      'hidden' => array(
        41 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'field_tt_course_date' => 'central',
      'group_your_details' => 'central',
      'field_address' => 'central',
      'group_emergency_contact' => 'central',
      'group_further_info' => 'central',
      'group_celta' => 'central',
      'field_extra_info' => 'central',
      'field_dyslexia' => 'central',
      'field_special_conditions' => 'central',
      'field_html_insurance' => 'central',
      'field_needs_and_requirements' => 'central',
      'field_lead_generation' => 'central',
      'field_marketing_other' => 'central',
      'field_offences' => 'central',
      'field_special_more_info' => 'central',
      'field_occupation' => 'central',
      'field_terms' => 'central',
      'group_work_experience' => 'central',
      'field_alternative_course_date' => 'central',
      'group_education' => 'central',
      'field_course_how' => 'central',
      'group_teaching_experience' => 'central',
      'field_accommodation_help' => 'central',
      'group_languages' => 'central',
      'field_emergency_name' => 'central',
      'field_emergency_telephone' => 'central',
      'field_secondary_education' => 'central',
      'field_higher_education' => 'central',
      'field_professional_training' => 'central',
      'field_experience_efl' => 'central',
      'field_experience_other_subject' => 'central',
      'field_experience_teacher_trainin' => 'central',
      'field_applicant' => 'central',
      'field_sex' => 'central',
      'field_date_of_birth' => 'central',
      'field_nationality' => 'central',
      'field_telephone' => 'central',
      'field_mobile_telephone' => 'central',
      'field_email' => 'central',
      'field_languages_known' => 'central',
      'field_work_experience' => 'central',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['entityform|teacher_training_application|form'] = $ds_layout;

  return $export;
}
