<?php
/**
 * @file
 * teacher_training_application..inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function teacher_training_application_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_applicant';
  $multifield->label = 'field_applicant';
  $multifield->description = '';
  $export['field_applicant'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_languages_known';
  $multifield->label = 'field_languages_known';
  $multifield->description = '';
  $export['field_languages_known'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_work_experience';
  $multifield->label = 'field_work_experience';
  $multifield->description = '';
  $export['field_work_experience'] = $multifield;

  return $export;
}
