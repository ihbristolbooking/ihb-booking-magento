<?php

/**
 * @file
 * entityform_registration.rules.inc
 */

/**
 * Implements hook_rules_action_info().
 */
function entityform_registration_rules_action_info() {
  return array(
    'entityform_registration_log_user_in' => array(
      'label' => 'Log in user',
      'parameter' => array(
        'account' => array('type' => 'user', 'label' => t('User')),
      ),
      'group' => t('Custom'),
    ),
  );
}

/**
 * Rules action callback to login the user provided in the function parameter.
 */
function entityform_registration_log_user_in($account) {

    global $user;

    if ( $user->uid ) {
      // Logged in user

    }
    else {
      // Not logged in

      global $user;
      $user = user_load($account->uid);
      user_login_finalize();

    }

}

/**
 * Implements hook_default_rules_configuration_alter().
 */
function entityform_registration_default_rules_configuration_alter(&$configs) {
  
   // dpm($configs);

   if($configs['custom']){
    $configs['custom']->action(rules_action('entityform_registration_log_user_in', array('account:select' => 'account-fetched:0')));
    }
}