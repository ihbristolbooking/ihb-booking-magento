<?php
/**
 * @file
 * Handles the layout of the gapped_sentence answering form.
 *
 *
 * Variables available:
 * - $form
 */
print drupal_render($form);

?>