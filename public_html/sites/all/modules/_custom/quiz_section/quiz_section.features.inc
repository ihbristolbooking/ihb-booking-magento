<?php
/**
 * @file
 * quiz_section.features.inc
 */

/**
 * Implements hook_views_api().
 */
function quiz_section_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
