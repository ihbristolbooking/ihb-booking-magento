<?php
/**
 * @file
 * ihbristol_online_tests.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ihbristol_online_tests_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "ds_extras" && $api == "ds_extras") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
