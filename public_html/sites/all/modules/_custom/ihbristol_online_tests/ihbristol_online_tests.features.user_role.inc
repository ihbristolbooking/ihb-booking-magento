<?php
/**
 * @file
 * ihbristol_online_tests.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ihbristol_online_tests_user_default_roles() {
  $roles = array();

  // Exported role: Quiz Administrator.
  $roles['Quiz Administrator'] = array(
    'name' => 'Quiz Administrator',
    'weight' => 3,
  );

  // Exported role: Quiz Participant.
  $roles['Quiz Participant'] = array(
    'name' => 'Quiz Participant',
    'weight' => 4,
  );

  return $roles;
}
