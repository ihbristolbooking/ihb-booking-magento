<?php
/**
 * @file
 * ihbristol_online_tests.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ihbristol_online_tests_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 0,
  'title' => 'GS1 - Computer Monitor',
  'log' => 'The current revision has been answered. We create a new revision so that the reports from the existing answers stays correct.',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '27f8976b-7133-425c-945a-2e5d4c1a0f7a',
  'ds_switch' => '',
  'type' => 'quiz',
  'language' => 'und',
  'created' => 1455644464,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0f7020dc-0803-4013-ac21-d2b2a99cb0b7',
  'revision_uid' => 1,
  'quiz' => array(
    'qnp_id' => 18,
    'number_of_random_questions' => 0,
    'max_score_for_random' => 1,
    'pass_rate' => 75,
    'summary_pass' => '',
    'summary_pass_format' => 'plain_text',
    'summary_default' => '',
    'summary_default_format' => 'plain_text',
    'randomization' => 0,
    'backwards_navigation' => 1,
    'keep_results' => 2,
    'repeat_until_correct' => 0,
    'quiz_open' => 1453828980,
    'quiz_close' => 1456420980,
    'takes' => 0,
    'show_attempt_stats' => 1,
    'time_limit' => 1800,
    'quiz_always' => 1,
    'tid' => 0,
    'has_userpoints' => 0,
    'userpoints_tid' => 0,
    'time_left' => 0,
    'max_score' => 30,
    'allow_skipping' => 1,
    'allow_resume' => 1,
    'allow_jumping' => 0,
    'allow_change' => 1,
    'allow_change_blank' => 0,
    'build_on_last' => '',
    'show_passed' => 1,
    'mark_doubtful' => 0,
    'review_options' => array(
      'question' => array(
        'quiz_question_view_full' => 0,
        'quiz_question_view_teaser' => 0,
        'attempt' => 0,
        'choice' => 0,
        'correct' => 0,
        'score' => 0,
        'answer_feedback' => 0,
        'question_feedback' => 0,
        'solution' => 0,
        'quiz_feedback' => 0,
      ),
      'end' => array(
        'quiz_question_view_full' => 'quiz_question_view_full',
        'attempt' => 'attempt',
        'choice' => 'choice',
        'quiz_question_view_teaser' => 0,
        'correct' => 0,
        'score' => 0,
        'answer_feedback' => 0,
        'question_feedback' => 0,
        'solution' => 0,
        'quiz_feedback' => 0,
      ),
    ),
    'rdf_mapping' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
  ),
  'qnp_id' => 18,
  'number_of_random_questions' => 0,
  'max_score_for_random' => 1,
  'pass_rate' => 75,
  'summary_pass' => '',
  'summary_pass_format' => 'plain_text',
  'summary_default' => '',
  'summary_default_format' => 'plain_text',
  'randomization' => 0,
  'backwards_navigation' => 1,
  'keep_results' => 2,
  'repeat_until_correct' => 0,
  'quiz_open' => 1453828980,
  'quiz_close' => 1456420980,
  'takes' => 0,
  'show_attempt_stats' => 1,
  'time_limit' => 1800,
  'quiz_always' => 1,
  'tid' => 0,
  'has_userpoints' => 0,
  'userpoints_tid' => 0,
  'time_left' => 0,
  'max_score' => 30,
  'allow_skipping' => 1,
  'allow_resume' => 1,
  'allow_jumping' => 0,
  'allow_change' => 1,
  'allow_change_blank' => 0,
  'build_on_last' => '',
  'show_passed' => 1,
  'mark_doubtful' => 0,
  'review_options' => array(
    'question' => array(
      'quiz_question_view_full' => 0,
      'quiz_question_view_teaser' => 0,
      'attempt' => 0,
      'choice' => 0,
      'correct' => 0,
      'score' => 0,
      'answer_feedback' => 0,
      'question_feedback' => 0,
      'solution' => 0,
      'quiz_feedback' => 0,
    ),
    'end' => array(
      'quiz_question_view_full' => 'quiz_question_view_full',
      'attempt' => 'attempt',
      'choice' => 'choice',
      'quiz_question_view_teaser' => 0,
      'correct' => 0,
      'score' => 0,
      'answer_feedback' => 0,
      'question_feedback' => 0,
      'solution' => 0,
      'quiz_feedback' => 0,
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'body' => array(),
  'field_quiz_category' => array(
    'und' => array(
      0 => array(
        'tid' => 102,
        'uuid' => '8be82743-19a5-4b44-b0e9-c43c6655cc9a',
      ),
    ),
  ),
  'field_quiz_exam_course_type' => array(),
  'field_quiz_level' => array(),
  'field_quiz_type' => array(
    'und' => array(
      0 => array(
        'tid' => 99,
        'uuid' => '41a16277-80b1-4092-8ba3-d0223c21de64',
      ),
    ),
  ),
  'field_entry_id' => array(),
  'field_ilearn' => array(),
  'field_not_logged_in' => array(),
  'field_time_mins' => array(),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-16 17:41:04 +0000',
);
  return $nodes;
}
