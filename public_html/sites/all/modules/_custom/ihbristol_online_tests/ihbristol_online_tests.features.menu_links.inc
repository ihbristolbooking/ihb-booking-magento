<?php
/**
 * @file
 * ihbristol_online_tests.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ihbristol_online_tests_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_score-keyword-transformation-questions:admin/quiz/reports/score-keyword-transformation.
  $menu_links['management_score-keyword-transformation-questions:admin/quiz/reports/score-keyword-transformation'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/quiz/reports/score-keyword-transformation',
    'router_path' => 'admin/quiz/reports/score-keyword-transformation',
    'link_title' => 'Score keyword transformation questions',
    'options' => array(
      'attributes' => array(
        'title' => 'Score the answers from quizzes that use short answer questions.',
      ),
      'identifier' => 'management_score-keyword-transformation-questions:admin/quiz/reports/score-keyword-transformation',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_quiz-reports-and-scoring:admin/quiz/reports',
  );
  // Exported menu link: management_score-word-formation-questions:admin/quiz/reports/score-word-formation.
  $menu_links['management_score-word-formation-questions:admin/quiz/reports/score-word-formation'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/quiz/reports/score-word-formation',
    'router_path' => 'admin/quiz/reports/score-word-formation',
    'link_title' => 'Score word formation questions',
    'options' => array(
      'attributes' => array(
        'title' => 'Score the answers from quizzes that use word formation questions.',
      ),
      'identifier' => 'management_score-word-formation-questions:admin/quiz/reports/score-word-formation',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_quiz-reports-and-scoring:admin/quiz/reports',
  );
  // Exported menu link: navigation_keyword-transformation-question:node/add/keyword-transformation.
  $menu_links['navigation_keyword-transformation-question:node/add/keyword-transformation'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/keyword-transformation',
    'router_path' => 'node/add/keyword-transformation',
    'link_title' => 'Keyword Transformation question',
    'options' => array(
      'attributes' => array(
        'title' => 'Quiz questions that allow a user to enter a line of text.',
      ),
      'identifier' => 'navigation_keyword-transformation-question:node/add/keyword-transformation',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_word-formation-question:node/add/word-formation.
  $menu_links['navigation_word-formation-question:node/add/word-formation'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/word-formation',
    'router_path' => 'node/add/word-formation',
    'link_title' => 'Word Formation question',
    'options' => array(
      'attributes' => array(
        'title' => 'Quiz questions that allow a user to enter a word.',
      ),
      'identifier' => 'navigation_word-formation-question:node/add/word-formation',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Keyword Transformation question');
  t('Score keyword transformation questions');
  t('Score word formation questions');
  t('Word Formation question');

  return $menu_links;
}
