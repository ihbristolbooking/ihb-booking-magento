<?php
/**
 * @file
 * ihbristol_online_tests.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function ihbristol_online_tests_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Open Cloze',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '1ca838dd-9734-41ea-8d31-31fe098803c2',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Grammar, Vocabulary & Phrases',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3a24bb78-f69f-4fbe-93b2-a2b273eecaa9',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Cambridge ESOL',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '41a16277-80b1-4092-8ba3-d0223c21de64',
    'vocabulary_machine_name' => 'quiz_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Grammar, Vocabulary & Phrases',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '460e49ba-f84c-4898-ab86-526dd29f0078',
    'vocabulary_machine_name' => 'quiz_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Word Formation',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '654c6fff-13ae-4eee-96a5-d4b4945420f2',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Gapped Sentences',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8be82743-19a5-4b44-b0e9-c43c6655cc9a',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Keyword Transformation',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8e3e5944-6c9c-4f17-9bab-93897eba1179',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Online Test',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a85203dd-8d51-4a50-8590-115b26bb3066',
    'vocabulary_machine_name' => 'quiz_type',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Phrases',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b8f19d59-06cf-4a24-a0fe-c0006e11447e',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Multiple Choice Cloze',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'c5348470-c95f-47b0-8232-3b857d6006b9',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Vocabulary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e519bcfc-b0de-4584-abb2-dcbe78f41ceb',
    'vocabulary_machine_name' => 'quiz_category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
