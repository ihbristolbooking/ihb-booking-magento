<?php
/**
 * @file
 * ihbristol_online_tests.ds_extras.inc
 */

/**
 * Implements hook_ds_vd_info().
 */
function ihbristol_online_tests_ds_vd_info() {
  $export = array();

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'free_exercises-page';
  $ds_vd->label = 'Free_exercises: Free Exercises Page (Views template)';
  $export['free_exercises-page'] = $ds_vd;

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'free_exercises-page_1';
  $ds_vd->label = 'Free_exercises: Free Exercises - esol (Views template)';
  $export['free_exercises-page_1'] = $ds_vd;

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'free_exercises-page_2';
  $ds_vd->label = 'Free_exercises: Free Exercises - grammar-vocabulary-sayings (Views template)';
  $export['free_exercises-page_2'] = $ds_vd;

  return $export;
}
