<?php
/**
 * @file
 * ihbristol_online_tests.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function ihbristol_online_tests_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|free_exercises-page_1|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'free_exercises-page_1';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'header' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_excises_login' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_esol_cae' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_esol_cpe' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_esol_fce' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|free_exercises-page_1|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|free_exercises-page_2|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'free_exercises-page_2';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'header' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_excises_login' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_advanced' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_elementary' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_intermediate' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|free_exercises-page_2|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|free_exercises-page|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'free_exercises-page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'free_exercises_esol_block' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'free_exercises_grammar_block' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|free_exercises-page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|useful_phrases-page_1|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'useful_phrases-page_1';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'header' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_advanced' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_elementary' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'useful_phrases_intermediate' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|useful_phrases-page_1|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function ihbristol_online_tests_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_excises_login';
  $ds_field->label = 'Free Excises Login';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p class="login-promo"><strong>Would you like to see some more exercises?</strong><br>
<a href="/users/register">Register to get the full selection</a> <em>or</em> <a href="/users/login">Login</a>.</p>',
      'format' => 'full_html',
    ),
    'use_token' => 0,
  );
  $export['free_excises_login'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_advanced';
  $ds_field->label = 'Free Exercises - Advanced';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_4',
    'block_render' => '2',
  );
  $export['free_exercises_advanced'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_elementary';
  $ds_field->label = 'Free Exercises - Elementary';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_2',
    'block_render' => '2',
  );
  $export['free_exercises_elementary'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_esol_block';
  $ds_field->label = 'Free Exercises - ESOL BLOCK';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'block|8',
    'block_render' => '2',
  );
  $export['free_exercises_esol_block'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_esol_cae';
  $ds_field->label = 'Free Exercises - ESOL - CAE';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_6',
    'block_render' => '1',
  );
  $export['free_exercises_esol_cae'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_esol_cpe';
  $ds_field->label = 'Free Exercises - ESOL - CPE';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_7',
    'block_render' => '2',
  );
  $export['free_exercises_esol_cpe'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_esol_fce';
  $ds_field->label = 'Free Exercises - ESOL - FCE';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_5',
    'block_render' => '2',
  );
  $export['free_exercises_esol_fce'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_grammar_block';
  $ds_field->label = 'Free Exercises - Grammar BLOCK';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'block|9',
    'block_render' => '2',
  );
  $export['free_exercises_grammar_block'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'free_exercises_intermediate';
  $ds_field->label = 'Free Exercises - Intermediate';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|free_exercises-block_3',
    'block_render' => '2',
  );
  $export['free_exercises_intermediate'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_advanced';
  $ds_field->label = 'Useful Phrases Advanced';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_3',
    'block_render' => '1',
  );
  $export['useful_phrases_advanced'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_elementary';
  $ds_field->label = 'Useful Phrases Elementary';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_1',
    'block_render' => '1',
  );
  $export['useful_phrases_elementary'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'useful_phrases_intermediate';
  $ds_field->label = 'Useful Phrases Intermediate';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'ds_views' => 'ds_views',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|useful_phrases-block_2',
    'block_render' => '1',
  );
  $export['useful_phrases_intermediate'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function ihbristol_online_tests_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|free_exercises-page_1|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'free_exercises-page_1';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_4_4_4_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'header',
        1 => 'free_excises_login',
      ),
      'left' => array(
        2 => 'free_exercises_esol_fce',
      ),
      'central' => array(
        3 => 'free_exercises_esol_cae',
      ),
      'right' => array(
        4 => 'free_exercises_esol_cpe',
      ),
    ),
    'fields' => array(
      'header' => 'top',
      'free_excises_login' => 'top',
      'free_exercises_esol_fce' => 'left',
      'free_exercises_esol_cae' => 'central',
      'free_exercises_esol_cpe' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'central' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['ds_views|free_exercises-page_1|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|free_exercises-page_2|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'free_exercises-page_2';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_4_4_4_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'header',
        1 => 'free_excises_login',
      ),
      'left' => array(
        2 => 'free_exercises_elementary',
      ),
      'central' => array(
        3 => 'free_exercises_intermediate',
      ),
      'right' => array(
        4 => 'free_exercises_advanced',
      ),
    ),
    'fields' => array(
      'header' => 'top',
      'free_excises_login' => 'top',
      'free_exercises_elementary' => 'left',
      'free_exercises_intermediate' => 'central',
      'free_exercises_advanced' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'central' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['ds_views|free_exercises-page_2|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|free_exercises-page|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'free_exercises-page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_6_6';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'free_exercises_grammar_block',
      ),
      'right' => array(
        1 => 'free_exercises_esol_block',
      ),
    ),
    'fields' => array(
      'free_exercises_grammar_block' => 'left',
      'free_exercises_esol_block' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['ds_views|free_exercises-page|default'] = $ds_layout;

  return $export;
}
