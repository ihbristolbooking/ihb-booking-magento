<?php
/**
 * @file
 * register_for_free_exercises.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function register_for_free_exercises_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_age'.
  $field_bases['field_age'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_age',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'age' => 'age',
        ),
      ),
      'target_type' => 'taxonomy_term',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_confirm_email'.
  $field_bases['field_confirm_email'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_confirm_email',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'email',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'email',
  );

  // Exported field_base: 'field_country'.
  $field_bases['field_country'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_country',
    'indexes' => array(
      'iso2' => array(
        0 => 'iso2',
      ),
    ),
    'locked' => 0,
    'module' => 'countries',
    'settings' => array(
      'continents' => array(
        'AF' => 'AF',
        'AN' => 'AN',
        'AS' => 'AS',
        'EU' => 'EU',
        'NA' => 'NA',
        'OC' => 'OC',
        'SA' => 'SA',
      ),
      'countries' => array(),
      'enabled' => 1,
      'size' => 5,
    ),
    'translatable' => 0,
    'type' => 'country',
  );

  return $field_bases;
}
