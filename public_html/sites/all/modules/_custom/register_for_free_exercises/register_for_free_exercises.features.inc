<?php
/**
 * @file
 * register_for_free_exercises.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function register_for_free_exercises_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_entityform_type().
 */
function register_for_free_exercises_default_entityform_type() {
  $items = array();
  $items['register_for_free_exercises'] = entity_import('entityform_type', '{
    "type" : "register_for_free_exercises",
    "label" : "Register for Free Exercises",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "full_html" },
      "submit_button_text" : "Register",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "full_html" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : 0, "3" : 0, "4" : 0, "5" : 0, "6" : 0, "7" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "free-english-exercises\\/",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003EFill out your details to register with us so you can do more free exercises\\u003C\\/p\\u003E\\r\\n",
        "format" : "full_html"
      },
      "enable_block" : 0
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/register-for-free-exercises",
        "alias" : "free-english-exercises\\/register",
        "language" : "und"
      }
    }
  }');
  return $items;
}
