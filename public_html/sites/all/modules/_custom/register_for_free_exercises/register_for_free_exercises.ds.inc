<?php
/**
 * @file
 * register_for_free_exercises.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function register_for_free_exercises_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'entityform|register_for_free_exercises|form';
  $ds_layout->entity_type = 'entityform';
  $ds_layout->bundle = 'register_for_free_exercises';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_6_6_bricks';
  $ds_layout->settings = array(
    'regions' => array(
      'topleft' => array(
        0 => 'field_first_name',
      ),
      'topright' => array(
        1 => 'field_surname',
      ),
      'bottomleft' => array(
        2 => 'field_email',
      ),
      'bottomright' => array(
        3 => 'field_confirm_email',
      ),
      'bottom' => array(
        4 => 'field_age',
        5 => 'field_country',
      ),
      'hidden' => array(
        6 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'field_first_name' => 'topleft',
      'field_surname' => 'topright',
      'field_email' => 'bottomleft',
      'field_confirm_email' => 'bottomright',
      'field_age' => 'bottom',
      'field_country' => 'bottom',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'topleft' => 'div',
      'topright' => 'div',
      'central' => 'div',
      'bottomleft' => 'div',
      'bottomright' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['entityform|register_for_free_exercises|form'] = $ds_layout;

  return $export;
}
