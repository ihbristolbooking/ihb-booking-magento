<?php
/**
 * @file
 * useful_phrases_and_expressions.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function useful_phrases_and_expressions_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'useful_expressions';
  $feeds_importer->config = array(
    'name' => 'Useful Expressions',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Url',
            'target' => 'url',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Introduction',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Category',
            'target' => 'field_category',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Expressions',
            'target' => 'field_expression_list',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Usage',
            'target' => 'field_usage_list',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'useful_expressions',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['useful_expressions'] = $feeds_importer;

  return $export;
}
