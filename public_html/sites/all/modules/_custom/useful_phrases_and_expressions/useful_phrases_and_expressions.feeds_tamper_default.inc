<?php
/**
 * @file
 * useful_phrases_and_expressions.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function useful_phrases_and_expressions_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'useful_expressions-category-explode';
  $feeds_tamper->importer = 'useful_expressions';
  $feeds_tamper->source = 'Category';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '*',
    'limit' => '',
    'real_separator' => '*',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['useful_expressions-category-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'useful_expressions-expressions-explode';
  $feeds_tamper->importer = 'useful_expressions';
  $feeds_tamper->source = 'Expressions';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '*',
    'limit' => '',
    'real_separator' => '*',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['useful_expressions-expressions-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'useful_expressions-image-find_replace';
  $feeds_tamper->importer = 'useful_expressions';
  $feeds_tamper->source = 'Image';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => '{filedir_2}',
    'replace' => 'sites/default/files/import_images/filedir_2/',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace';
  $export['useful_expressions-image-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'useful_expressions-usage-explode';
  $feeds_tamper->importer = 'useful_expressions';
  $feeds_tamper->source = 'Usage';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '*',
    'limit' => '',
    'real_separator' => '*',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['useful_expressions-usage-explode'] = $feeds_tamper;

  return $export;
}
