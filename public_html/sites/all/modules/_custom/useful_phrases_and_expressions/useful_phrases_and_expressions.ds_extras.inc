<?php
/**
 * @file
 * useful_phrases_and_expressions.ds_extras.inc
 */

/**
 * Implements hook_ds_vd_info().
 */
function useful_phrases_and_expressions_ds_vd_info() {
  $export = array();

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'useful_phrases-page_1';
  $ds_vd->label = 'Useful_phrases: Page (Views template)';
  $export['useful_phrases-page_1'] = $ds_vd;

  return $export;
}
