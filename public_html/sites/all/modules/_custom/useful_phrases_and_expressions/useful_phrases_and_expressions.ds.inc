<?php
/**
 * @file
 * useful_phrases_and_expressions.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function useful_phrases_and_expressions_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|useful_phrases-page_1|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'useful_phrases-page_1';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_4_4_4_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'header',
      ),
      'left' => array(
        1 => 'useful_phrases_elementary',
      ),
      'central' => array(
        2 => 'useful_phrases_intermediate',
      ),
      'right' => array(
        3 => 'useful_phrases_advanced',
      ),
    ),
    'fields' => array(
      'header' => 'top',
      'useful_phrases_elementary' => 'left',
      'useful_phrases_intermediate' => 'central',
      'useful_phrases_advanced' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'central' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['ds_views|useful_phrases-page_1|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|useful_expressions|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'useful_expressions';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_6_6_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'field_category',
        1 => 'field_image',
        2 => 'body',
      ),
      'left' => array(
        3 => 'field_expression_list',
      ),
      'right' => array(
        4 => 'field_usage_list',
      ),
    ),
    'fields' => array(
      'field_category' => 'top',
      'field_image' => 'top',
      'body' => 'top',
      'field_expression_list' => 'left',
      'field_usage_list' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|useful_expressions|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|useful_expressions|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'useful_expressions';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_category',
        3 => 'field_expression_list',
        4 => 'field_usage_list',
        5 => 'field_image',
        6 => 'path',
      ),
      'hidden' => array(
        7 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'central',
      'body' => 'central',
      'field_category' => 'central',
      'field_expression_list' => 'central',
      'field_usage_list' => 'central',
      'field_image' => 'central',
      'path' => 'central',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|useful_expressions|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|useful_expressions|listing';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'useful_expressions';
  $ds_layout->view_mode = 'listing';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'title',
      ),
    ),
    'fields' => array(
      'title' => 'central',
    ),
    'classes' => array(),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|useful_expressions|listing'] = $ds_layout;

  return $export;
}
