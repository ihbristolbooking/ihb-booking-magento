<?php
/**
 * @file
 * useful_phrases_and_expressions.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function useful_phrases_and_expressions_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Intermediate',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '95865692-230d-496d-b32c-8337219fca9d',
    'vocabulary_machine_name' => 'category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Advanced',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '9a41ab4d-a841-4425-be00-303149d55db8',
    'vocabulary_machine_name' => 'category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Elementary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'c98c17eb-89bd-458e-a061-730cf55e7f29',
    'vocabulary_machine_name' => 'category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
