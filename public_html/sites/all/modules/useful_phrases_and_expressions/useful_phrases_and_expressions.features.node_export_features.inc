<?php
/**
 * @file
 * useful_phrases_and_expressions.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function useful_phrases_and_expressions_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'13764\',
      \'uid\' => \'1\',
      \'title\' => \'Apologising\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'c3d5d7e4-2978-4eec-a482-135ee4ffd423\',
      \'ds_switch\' => \'\',
      \'nid\' => \'13676\',
      \'type\' => \'useful_expressions\',
      \'language\' => \'und\',
      \'created\' => \'1455528548\',
      \'changed\' => \'1455528631\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'6ac340cc-8079-4a36-a423-ea2b85fccafc\',
      \'revision_timestamp\' => \'1455528631\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Everybody makes mistakes sometimes. When it happens we need a phrase to tell the other person how really sorry we are and stop them getting really angry. Here are ten phrases.\',
            \'summary\' => \'\',
            \'format\' => \'filtered_html\',
            \'safe_value\' => "<p>Everybody makes mistakes sometimes. When it happens we need a phrase to tell the other person how really sorry we are and stop them getting really angry. Here are ten phrases.</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'field_category\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'78\',
          ),
        ),
      ),
      \'field_expression_list\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Sorry.\',
            \'format\' => NULL,
            \'safe_value\' => \'Sorry.\',
          ),
          array(
            \'value\' => "I\'m (so / very / terribly) sorry.",
            \'format\' => NULL,
            \'safe_value\' => \'I&#039;m (so / very / terribly) sorry.\',
          ),
          array(
            \'value\' => \'Ever so sorry.\',
            \'format\' => NULL,
            \'safe_value\' => \'Ever so sorry.\',
          ),
          array(
            \'value\' => \'How stupid / careless / thoughtless of me.\',
            \'format\' => NULL,
            \'safe_value\' => \'How stupid / careless / thoughtless of me.\',
          ),
          array(
            \'value\' => \'Pardon (me)\',
            \'format\' => NULL,
            \'safe_value\' => \'Pardon (me)\',
          ),
          array(
            \'value\' => "That\'s my fault.",
            \'format\' => NULL,
            \'safe_value\' => \'That&#039;s my fault.\',
          ),
          array(
            \'value\' => \'Sorry. It was all my fault.\',
            \'format\' => NULL,
            \'safe_value\' => \'Sorry. It was all my fault.\',
          ),
          array(
            \'value\' => \'Please excuse my (ignorance)\',
            \'format\' => NULL,
            \'safe_value\' => \'Please excuse my (ignorance)\',
          ),
          array(
            \'value\' => "Please don\'t be mad at me.",
            \'format\' => NULL,
            \'safe_value\' => \'Please don&#039;t be mad at me.\',
          ),
          array(
            \'value\' => \'Please accept our (sincerest) apologies.\',
            \'format\' => NULL,
            \'safe_value\' => \'Please accept our (sincerest) apologies.\',
          ),
        ),
      ),
      \'field_usage_list\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Phrase 1 is a general short apology. We use this when we bump into people on the street. At other times, it sounds too weak.\',
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 1 is a general short apology. We use this when we bump into people on the street. At other times, it sounds too weak.\',
          ),
          array(
            \'value\' => "In phrase 2, we use \'so\', \'very\' and \'terribly\' to make the meaning stronger. \'Terribly\' is the strongest. If we use one of the words in brackets, it is stressed.",
            \'format\' => NULL,
            \'safe_value\' => \'In phrase 2, we use &#039;so&#039;, &#039;very&#039; and &#039;terribly&#039; to make the meaning stronger. &#039;Terribly&#039; is the strongest. If we use one of the words in brackets, it is stressed.\',
          ),
          array(
            \'value\' => "Phrase 3 is quite formal but it\'s a stronger apology than just \'sorry\'.",
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 3 is quite formal but it&#039;s a stronger apology than just &#039;sorry&#039;.\',
          ),
          array(
            \'value\' => \'We use phrase 4 to criticise ourselves and the mistake that we have just made.\',
            \'format\' => NULL,
            \'safe_value\' => \'We use phrase 4 to criticise ourselves and the mistake that we have just made.\',
          ),
          array(
            \'value\' => \'We use phrases 6 and 7 to take all the responsibility for what happened. Phrase 7 is a little stronger.\',
            \'format\' => NULL,
            \'safe_value\' => \'We use phrases 6 and 7 to take all the responsibility for what happened. Phrase 7 is a little stronger.\',
          ),
          array(
            \'value\' => \'We use phrase 8 to apologise for our lack of knowledge or ability. We can replace the word in brackets with other nouns, e.g. carelessness, forgetfulness.\',
            \'format\' => NULL,
            \'safe_value\' => \'We use phrase 8 to apologise for our lack of knowledge or ability. We can replace the word in brackets with other nouns, e.g. carelessness, forgetfulness.\',
          ),
          array(
            \'value\' => \'Phrase 9 is asking the other person not to get angry. The tone is quite informal.\',
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 9 is asking the other person not to get angry. The tone is quite informal.\',
          ),
          array(
            \'value\' => "Phrase 10 is often used in formal letters. The word \'sincerest\' makes the apology very strong and very formal.",
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 10 is often used in formal letters. The word &#039;sincerest&#039; makes the apology very strong and very formal.\',
          ),
        ),
      ),
      \'field_image\' => array(),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'path\' => array(
        \'pid\' => \'574\',
        \'source\' => \'node/13676\',
        \'alias\' => \'useful-english-expressions/example/apologising\',
        \'language\' => \'und\',
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1455528548\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'13765\',
      \'uid\' => \'1\',
      \'title\' => \'Asking for approval\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'6b5acd8e-3e7d-49c2-b71c-c451be0c7abb\',
      \'ds_switch\' => \'\',
      \'nid\' => \'13677\',
      \'type\' => \'useful_expressions\',
      \'language\' => \'und\',
      \'created\' => \'1455529452\',
      \'changed\' => \'1455529452\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'b0e3ee39-5129-459f-a824-c8e8effb3660\',
      \'revision_timestamp\' => \'1455529452\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "Sometimes we are not sure if it\'s a good idea to do something. So we need useful expressions for asking if other people agree with an idea or intended action. Here are ten phrases.",
            \'summary\' => \'\',
            \'format\' => \'filtered_html\',
            \'safe_value\' => "<p>Sometimes we are not sure if it\'s a good idea to do something. So we need useful expressions for asking if other people agree with an idea or intended action. Here are ten phrases.</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'field_category\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'79\',
          ),
        ),
      ),
      \'field_expression_list\' => array(
        \'und\' => array(
          array(
            \'value\' => "Do you think it\'s all right to do it?",
            \'format\' => NULL,
            \'safe_value\' => \'Do you think it&#039;s all right to do it?\',
          ),
          array(
            \'value\' => \'What do you think about (me doing that)?\',
            \'format\' => NULL,
            \'safe_value\' => \'What do you think about (me doing that)?\',
          ),
          array(
            \'value\' => \'Do you think / reckon I ought to (do it)?\',
            \'format\' => NULL,
            \'safe_value\' => \'Do you think / reckon I ought to (do it)?\',
          ),
          array(
            \'value\' => \'What would you say if I (did it)?\',
            \'format\' => NULL,
            \'safe_value\' => \'What would you say if I (did it)?\',
          ),
          array(
            \'value\' => \'Would you approve of (doing something)?\',
            \'format\' => NULL,
            \'safe_value\' => \'Would you approve of (doing something)?\',
          ),
          array(
            \'value\' => \'What is your attitude to the idea of...\',
            \'format\' => NULL,
            \'safe_value\' => \'What is your attitude to the idea of...\',
          ),
          array(
            \'value\' => \'Are you in favour of (me doing something)?\',
            \'format\' => NULL,
            \'safe_value\' => \'Are you in favour of (me doing something)?\',
          ),
          array(
            \'value\' => "You are in favour of ... aren\'t you?",
            \'format\' => NULL,
            \'safe_value\' => \'You are in favour of ... aren&#039;t you?\',
          ),
          array(
            \'value\' => \'Do you think anyone would mind if I...\',
            \'format\' => NULL,
            \'safe_value\' => \'Do you think anyone would mind if I...\',
          ),
          array(
            \'value\' => \'Do you think it would be really awful if I\',
            \'format\' => NULL,
            \'safe_value\' => \'Do you think it would be really awful if I\',
          ),
        ),
      ),
      \'field_usage_list\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Phrases 1, 2 and 3 are quite informal ways of asking if another person agrees with an action that you are planning to do.\',
            \'format\' => NULL,
            \'safe_value\' => \'Phrases 1, 2 and 3 are quite informal ways of asking if another person agrees with an action that you are planning to do.\',
          ),
          array(
            \'value\' => "Phrases 4 and 5 are hypothetical and so sound a bit more polite. Phrases 1 to 3 suggest that speaker probably will do it. 4 and 5 suggest that the speaker won\'t do it if another person doesn\'t agree.",
            \'format\' => NULL,
            \'safe_value\' => \'Phrases 4 and 5 are hypothetical and so sound a bit more polite. Phrases 1 to 3 suggest that speaker probably will do it. 4 and 5 suggest that the speaker won&#039;t do it if another person doesn&#039;t agree.\',
          ),
          array(
            \'value\' => "Phrase 6 is asking another person for their feelings about an imagined action. The speaker doesn\'t actually say that she is thinking of doing it so is making the action more remote. This phrase is quite formal.",
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 6 is asking another person for their feelings about an imagined action. The speaker doesn&#039;t actually say that she is thinking of doing it so is making the action more remote. This phrase is quite formal.\',
          ),
          array(
            \'value\' => "In phrase 7, the action being described will seem more remote if the speaker leaves out the word \'me\', in a similar way to phrase 6.",
            \'format\' => NULL,
            \'safe_value\' => \'In phrase 7, the action being described will seem more remote if the speaker leaves out the word &#039;me&#039;, in a similar way to phrase 6.\',
          ),
          array(
            \'value\' => "In phrase 8, it is important that the intonation is falling on the first \'are\' and is rising on the negative verb in the question tag \'aren\'t\'.",
            \'format\' => NULL,
            \'safe_value\' => \'In phrase 8, it is important that the intonation is falling on the first &#039;are&#039; and is rising on the negative verb in the question tag &#039;aren&#039;t&#039;.\',
          ),
          array(
            \'value\' => "Phrase 9 is quite informal and is asking the listener\'s opinion about other people\'s reactions. You follow this phrase with a past tense.",
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 9 is quite informal and is asking the listener&#039;s opinion about other people&#039;s reactions. You follow this phrase with a past tense.\',
          ),
          array(
            \'value\' => \'Phrase 10 is suggesting that the speaker expects that the other person will not agree but wants to know how strongly the other person disagress with the intended action.\',
            \'format\' => NULL,
            \'safe_value\' => \'Phrase 10 is suggesting that the speaker expects that the other person will not agree but wants to know how strongly the other person disagress with the intended action.\',
          ),
        ),
      ),
      \'field_image\' => array(),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'path\' => array(
        \'pid\' => \'575\',
        \'source\' => \'node/13677\',
        \'alias\' => \'useful-english-expressions/example/asking-approval\',
        \'language\' => \'und\',
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1455529452\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'13766\',
      \'uid\' => \'1\',
      \'title\' => "I\'m from Bristol",
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'de5b9991-1869-4c1b-af5b-2f826b327b88\',
      \'ds_switch\' => \'\',
      \'nid\' => \'13678\',
      \'type\' => \'useful_expressions\',
      \'language\' => \'und\',
      \'created\' => \'1455529706\',
      \'changed\' => \'1455529706\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'c7f5b17e-c065-4f0d-b752-84ee30eed967\',
      \'revision_timestamp\' => \'1455529706\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'We often tell people where we are from when we meet them. Here are different ways of doing that.\',
            \'summary\' => \'\',
            \'format\' => \'filtered_html\',
            \'safe_value\' => "<p>We often tell people where we are from when we meet them. Here are different ways of doing that.</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'field_category\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'80\',
          ),
        ),
      ),
      \'field_expression_list\' => array(
        \'und\' => array(
          array(
            \'value\' => \'I come from Bristol.\',
            \'format\' => NULL,
            \'safe_value\' => \'I come from Bristol.\',
          ),
          array(
            \'value\' => "I\'m from Bristol.",
            \'format\' => NULL,
            \'safe_value\' => \'I&#039;m from Bristol.\',
          ),
          array(
            \'value\' => "I\'m a Bristolian.",
            \'format\' => NULL,
            \'safe_value\' => \'I&#039;m a Bristolian.\',
          ),
          array(
            \'value\' => "Bristol\'s where I\'m from.",
            \'format\' => NULL,
            \'safe_value\' => \'Bristol&#039;s where I&#039;m from.\',
          ),
          array(
            \'value\' => "Bristol\'s my hometown.",
            \'format\' => NULL,
            \'safe_value\' => \'Bristol&#039;s my hometown.\',
          ),
          array(
            \'value\' => \'I was born and bred in Bristol.\',
            \'format\' => NULL,
            \'safe_value\' => \'I was born and bred in Bristol.\',
          ),
          array(
            \'value\' => "I\'m a Bristol boy/girl.",
            \'format\' => NULL,
            \'safe_value\' => \'I&#039;m a Bristol boy/girl.\',
          ),
          array(
            \'value\' => "Bristol\'s my home.",
            \'format\' => NULL,
            \'safe_value\' => \'Bristol&#039;s my home.\',
          ),
          array(
            \'value\' => \'I call Bristol home.\',
            \'format\' => NULL,
            \'safe_value\' => \'I call Bristol home.\',
          ),
          array(
            \'value\' => "I\'m based in Bristol.",
            \'format\' => NULL,
            \'safe_value\' => \'I&#039;m based in Bristol.\',
          ),
        ),
      ),
      \'field_usage_list\' => array(
        \'und\' => array(
          array(
            \'value\' => \'1 and 2 are both very common ways of giving this information.\',
            \'format\' => NULL,
            \'safe_value\' => \'1 and 2 are both very common ways of giving this information.\',
          ),
          array(
            \'value\' => \'3. Many cities can be used like this. Other examples are Bathonian from Bath, a Londoner from London, a Mancunian from Manchester.\',
            \'format\' => NULL,
            \'safe_value\' => \'3. Many cities can be used like this. Other examples are Bathonian from Bath, a Londoner from London, a Mancunian from Manchester.\',
          ),
          array(
            \'value\' => \'4. is a version of 4, and puts the emphasis on the place, not on you.\',
            \'format\' => NULL,
            \'safe_value\' => \'4. is a version of 4, and puts the emphasis on the place, not on you.\',
          ),
          array(
            \'value\' => "5. We all have a hometown - even if we don\'t live in it now.",
            \'format\' => NULL,
            \'safe_value\' => \'5. We all have a hometown - even if we don&#039;t live in it now.\',
          ),
          array(
            \'value\' => "6. If you are \'born and bred\' in a place it means you were born and grew up there.",
            \'format\' => NULL,
            \'safe_value\' => \'6. If you are &#039;born and bred&#039; in a place it means you were born and grew up there.\',
          ),
          array(
            \'value\' => \'7. A Bristol boy/girl is used even with adults. It means you spent your younger years in a place.\',
            \'format\' => NULL,
            \'safe_value\' => \'7. A Bristol boy/girl is used even with adults. It means you spent your younger years in a place.\',
          ),
          array(
            \'value\' => "8 and 9 are similar. We use these expressions to talk about where we live even if it\'s different from where we were born.",
            \'format\' => NULL,
            \'safe_value\' => \'8 and 9 are similar. We use these expressions to talk about where we live even if it&#039;s different from where we were born.\',
          ),
          array(
            \'value\' => \'10 is more common when we are living somewhere, maybe for a period of time, but it is not where we were born. If you change where you live because of your work, this is a useful structure.\',
            \'format\' => NULL,
            \'safe_value\' => \'10 is more common when we are living somewhere, maybe for a period of time, but it is not where we were born. If you change where you live because of your work, this is a useful structure.\',
          ),
        ),
      ),
      \'field_image\' => array(),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'path\' => array(
        \'pid\' => \'576\',
        \'source\' => \'node/13678\',
        \'alias\' => \'useful-english-expressions/example/im-bristol\',
        \'language\' => \'und\',
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1455529706\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
