<?php
/**
 * @file
 * useful_phrases_and_expressions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function useful_phrases_and_expressions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "ds_extras" && $api == "ds_extras") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function useful_phrases_and_expressions_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function useful_phrases_and_expressions_node_info() {
  $items = array(
    'useful_expressions' => array(
      'name' => t('Useful Expressions'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
